package com.ifwaxtel.reciprada.listeners;

import android.view.View;

import com.ifwaxtel.reciprada.remote.response.UserResponse;

import retrofit2.Response;

/**
 * The interface Auth listener.
 */
public interface AuthListener {
    /**
     * The interface On fragment interaction listener.
     */
    interface OnFragmentInteractionListener {
        /**
         * On fragment interaction.
         *
         * @param view the view
         */
        void onFragmentInteraction(View view);
    }

    /**
     * The interface Remote response interface.
     */
    interface RemoteResponseInterface{
        /**
         * On response.
         *
         * @param response the response
         */
        void onResponse(Response<UserResponse> response);

        /**
         * On failure.
         *
         * @param t the t
         */
        void onFailure(Throwable t);
    }

}
