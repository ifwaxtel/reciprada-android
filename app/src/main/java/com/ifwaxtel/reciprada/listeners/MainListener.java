package com.ifwaxtel.reciprada.listeners;

/**
 * The interface Main listener.
 */
public interface MainListener {
    /**
     * The interface On fragment interaction listener.
     */
    interface OnFragmentInteractionListener {
        /**
         * On fragment interaction.
         *
         * @param id the id
         */
        void onFragmentInteraction(int id);
    }
}
