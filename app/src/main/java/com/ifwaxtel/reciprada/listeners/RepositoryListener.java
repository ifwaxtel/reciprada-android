package com.ifwaxtel.reciprada.listeners;

/**
 * The interface Repository listener.
 */
public interface RepositoryListener {

    /**
     * The interface Token retrieve.
     */
    interface TokenRetrieve{
        /**
         * On fetch token success.
         *
         * @param token the token
         */
        void onFetchTokenSuccess(String token);

        /**
         * On fetch failed.
         *
         * @param response the response
         */
        void onFetchFailed(String response);
    }

    /**
     * The interface Categories retrieve.
     */
    interface CategoriesRetrieve{
        /**
         * On fetch categories success.
         */
        void onFetchCategoriesSuccess();

        /**
         * On fetch failed.
         *
         * @param response the response
         */
        void onFetchFailed(String response);
    }

    /**
     * The interface Recipes retrieve.
     */
    interface RecipesRetrieve{
        /**
         * On fetch recipes success.
         */
        void onFetchRecipesSuccess();

        /**
         * On fetch failed.
         *
         * @param response the response
         */
        void onFetchFailed(String response);
    }

    /**
     * The interface Ingredients retrieve.
     */
    interface IngredientsRetrieve{
        /**
         * On fetch ingredients success.
         */
        void onFetchIngredientsSuccess();

        /**
         * On fetch failed.
         *
         * @param response the response
         */
        void onFetchFailed(String response);
    }

    /**
     * The interface Tools retrieve.
     */
    interface ToolsRetrieve{
        /**
         * On fetch tools success.
         */
        void onFetchToolsSuccess();

        /**
         * On fetch failed.
         *
         * @param response the response
         */
        void onFetchFailed(String response);
    }



}
