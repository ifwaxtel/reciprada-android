package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Tool;

import java.util.List;

/**
 * The interface Tool dao.
 */
@Dao
public interface ToolDao {
    /**
     * Insert tool.
     *
     * @param tool the tool
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTool(Tool tool);

    /**
     * Load tools live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM " + GlobalConstants.TABLE_TOOLS)
    LiveData<List<Tool>> loadTools();

    /**
     * Load tool tool.
     *
     * @param id the id
     * @return the tool
     */
    @Query("SELECT * FROM tools WHERE id = :id LIMIT 1")
    Tool loadTool(int id);
}
