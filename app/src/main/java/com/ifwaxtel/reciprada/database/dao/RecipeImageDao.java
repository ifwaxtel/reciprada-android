package com.ifwaxtel.reciprada.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.RecipeImage;

import java.util.List;

/**
 * The interface Recipe image dao.
 */
@Dao
public interface RecipeImageDao {
    /**
     * Insert recipe image.
     *
     * @param recipeImage the recipe image
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecipeImage(RecipeImage recipeImage);

    /**
     * Load images for recipe list.
     *
     * @param recipeId the recipe id
     * @return the list
     */
    @Query("SELECT * FROM recipe_images WHERE recipe_id = :recipeId")
    List<RecipeImage> loadImagesForRecipe(int recipeId);
}
