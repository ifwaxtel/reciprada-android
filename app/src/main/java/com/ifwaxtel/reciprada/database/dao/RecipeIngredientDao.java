package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.RecipeIngredient;

import java.util.List;

/**
 * The interface Recipe ingredient dao.
 */
@Dao
public interface RecipeIngredientDao {
    /**
     * Insert recipe ingredient.
     *
     * @param recipeIngredient the recipe ingredient
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecipeIngredient(RecipeIngredient recipeIngredient);

    /**
     * Load recipe ingredients live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    @Query("SELECT * FROM recipe_ingredients WHERE recipe_id = :recipeId")
    LiveData<List<RecipeIngredient>> loadRecipeIngredients(int recipeId);

}
