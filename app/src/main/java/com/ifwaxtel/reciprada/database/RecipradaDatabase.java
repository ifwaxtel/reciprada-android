package com.ifwaxtel.reciprada.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.database.dao.CategoryDao;
import com.ifwaxtel.reciprada.database.dao.IngredientDao;
import com.ifwaxtel.reciprada.database.dao.RecipeDao;
import com.ifwaxtel.reciprada.database.dao.RecipeImageDao;
import com.ifwaxtel.reciprada.database.dao.RecipeIngredientDao;
import com.ifwaxtel.reciprada.database.dao.RecipeStepDao;
import com.ifwaxtel.reciprada.database.dao.RecipeToolDao;
import com.ifwaxtel.reciprada.database.dao.ToolDao;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Ingredient;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.core.model.RecipeIngredient;
import com.ifwaxtel.reciprada.core.model.RecipeStep;
import com.ifwaxtel.reciprada.core.model.RecipeTool;
import com.ifwaxtel.reciprada.core.model.Tool;

/**
 * The type Reciprada database.
 */
@Database(entities = {Category.class, Ingredient.class, Tool.class, Recipe.class, RecipeImage.class, RecipeIngredient.class, RecipeStep.class, RecipeTool.class},version = 7)
public abstract class RecipradaDatabase extends RoomDatabase {
    private static RecipradaDatabase INSTANCE;

    /**
     * Category dao category dao.
     *
     * @return the category dao
     */
    public abstract CategoryDao categoryDao();

    /**
     * Ingredient dao ingredient dao.
     *
     * @return the ingredient dao
     */
    public abstract IngredientDao ingredientDao();

    /**
     * Tool dao tool dao.
     *
     * @return the tool dao
     */
    public abstract ToolDao toolDao();

    /**
     * Recipe dao recipe dao.
     *
     * @return the recipe dao
     */
    public abstract RecipeDao recipeDao();

    /**
     * Recipe image dao recipe image dao.
     *
     * @return the recipe image dao
     */
    public abstract RecipeImageDao recipeImageDao();

    /**
     * Recipe ingredient dao recipe ingredient dao.
     *
     * @return the recipe ingredient dao
     */
    public abstract RecipeIngredientDao recipeIngredientDao();

    /**
     * Recipe step dao recipe step dao.
     *
     * @return the recipe step dao
     */
    public abstract RecipeStepDao recipeStepDao();

    /**
     * Recipe tool dao recipe tool dao.
     *
     * @return the recipe tool dao
     */
    public abstract RecipeToolDao recipeToolDao();

    /**
     * Gets database.
     *
     * @param context the context
     * @return the database
     */
    public static RecipradaDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RecipradaDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RecipradaDatabase.class, GlobalConstants.DB_NAME)
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
