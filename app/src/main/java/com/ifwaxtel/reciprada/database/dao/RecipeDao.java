package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Recipe;

import java.util.List;

/**
 * The interface Recipe dao.
 */
@Dao
public interface RecipeDao {
    /**
     * Insert recipe.
     *
     * @param recipe the recipe
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertRecipe(Recipe recipe);

    /**
     * Load recipes live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM recipes ORDER BY id DESC LIMIT 11")
    LiveData<List<Recipe>> loadRecipes();

    /**
     * Load recipes source data source . factory.
     *
     * @return the data source . factory
     */
    @Query("SELECT * FROM recipes ORDER BY id DESC")
    DataSource.Factory<Integer, Recipe> loadRecipesSource();

    /**
     * Load more recipes list.
     *
     * @param lastRecipeId the last recipe id
     * @return the list
     */
    @Query("SELECT * FROM recipes WHERE id < :lastRecipeId ORDER BY id DESC LIMIT 11")
    List<Recipe> loadMoreRecipes(int lastRecipeId);

    /**
     * Load recipes by category data source . factory.
     *
     * @param categoryId the category id
     * @return the data source . factory
     */
    @Query("SELECT * FROM recipes WHERE category_id = :categoryId ORDER BY id DESC")
    DataSource.Factory<Integer, Recipe> loadRecipesByCategory(int categoryId);

    /**
     * Load more recipes by category list.
     *
     * @param categoryId   the category id
     * @param lastRecipeId the last recipe id
     * @return the list
     */
    @Query("SELECT * FROM recipes WHERE category_id = :categoryId AND id < :lastRecipeId ORDER BY id DESC LIMIT 11")
    List<Recipe> loadMoreRecipesByCategory(int categoryId, int lastRecipeId);

    /**
     * Load favourite recipes data source . factory.
     *
     * @return the data source . factory
     */
    @Query("SELECT * FROM recipes WHERE favourite = 1 ORDER BY id DESC")
    DataSource.Factory<Integer, Recipe> loadFavouriteRecipes();

    /**
     * Load live favourite recipes live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM recipes WHERE favourite = 1 ORDER BY id DESC")
    LiveData<List<Recipe>> loadLiveFavouriteRecipes();

    /**
     * Load more favourite recipes list.
     *
     * @param lastRecipeId the last recipe id
     * @return the list
     */
    @Query("SELECT * FROM recipes WHERE favourite = 1 AND id < :lastRecipeId ORDER BY id DESC LIMIT 11")
    List<Recipe> loadMoreFavouriteRecipes(int lastRecipeId);

    /**
     * Load all recipes list.
     *
     * @return the list
     */
    @Query("SELECT * FROM recipes ORDER BY id DESC LIMIT 11")
    List<Recipe> loadAllRecipes();

    /**
     * Gets recipes count.
     *
     * @return the recipes count
     */
    @Query("SELECT COUNT(id) FROM recipes WHERE loadedFrom = '" + GlobalConstants.LOADED_FROM_RECIPE_LIST +"'")
    int getRecipesCount();

    /**
     * Gets recipes by category count.
     *
     * @param categoryId the category id
     * @return the recipes by category count
     */
    @Query("SELECT COUNT(id) FROM recipes WHERE category_id = :categoryId AND loadedFrom = '" + GlobalConstants.LOADED_FROM_RECIPE_CATEGORY + "'")
    int getRecipesByCategoryCount(int categoryId);

    /**
     * Update recipe.
     *
     * @param recipe the recipe
     */
    @Update
    void updateRecipe(Recipe recipe);
}
