package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.Ingredient;

import java.util.List;

/**
 * The interface Ingredient dao.
 */
@Dao
public interface IngredientDao {
    /**
     * Insert ingredient.
     *
     * @param ingredient the ingredient
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIngredient(Ingredient ingredient);

    /**
     * Load changed ingredients live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM ingredients")
    LiveData<List<Ingredient>> loadChangedIngredients();

    /**
     * Load ingredient ingredient.
     *
     * @param id the id
     * @return the ingredient
     */
    @Query("SELECT * FROM ingredients WHERE id = :id LIMIT 1")
    Ingredient loadIngredient(int id);


}
