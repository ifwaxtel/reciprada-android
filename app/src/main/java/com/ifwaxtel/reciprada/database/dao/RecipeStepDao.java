package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.RecipeStep;

import java.util.List;

/**
 * The interface Recipe step dao.
 */
@Dao
public interface RecipeStepDao {
    /**
     * Insert recipe step.
     *
     * @param recipeStep the recipe step
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecipeStep(RecipeStep recipeStep);

    /**
     * Load recipe steps live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    @Query("SELECT * FROM recipe_steps WHERE recipe_id = :recipeId")
    LiveData<List<RecipeStep>> loadRecipeSteps(int recipeId);

}
