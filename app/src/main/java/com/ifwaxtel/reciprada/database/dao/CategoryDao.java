package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.Category;

import java.util.List;

/**
 * The interface Category dao.
 */
@Dao
public interface CategoryDao {
    /**
     * Insert category.
     *
     * @param category the category
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategory(Category category);

    /**
     * Loadhome categories live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM categories ORDER BY id DESC LIMIT 5")
    LiveData<List<Category>> loadhomeCategories();

    /**
     * Load categories live data.
     *
     * @return the live data
     */
    @Query("SELECT * FROM categories ORDER BY id DESC LIMIT 10")
    LiveData<List<Category>> loadCategories();

    /**
     * Load more categories list.
     *
     * @param lastCategoryId the last category id
     * @return the list
     */
    @Query("SELECT * FROM categories WHERE id < :lastCategoryId ORDER BY id DESC LIMIT 10")
    List<Category> loadMoreCategories(int lastCategoryId);

    /**
     * Load recipe category category.
     *
     * @param categoryId the category id
     * @return the category
     */
    @Query("SELECT * FROM categories WHERE id = :categoryId LIMIT 1")
    Category loadRecipeCategory(int categoryId);

    /**
     * Gets categories count.
     *
     * @return the categories count
     */
    @Query("SELECT COUNT(id) FROM categories")
    int getCategoriesCount();
}
