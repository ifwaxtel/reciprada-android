package com.ifwaxtel.reciprada.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.ifwaxtel.reciprada.core.model.RecipeTool;

import java.util.List;

/**
 * The interface Recipe tool dao.
 */
@Dao
public interface RecipeToolDao {
    /**
     * Insert recipe tool.
     *
     * @param recipeTool the recipe tool
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRecipeTool(RecipeTool recipeTool);

    /**
     * Load recipe tools live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    @Query("SELECT * FROM recipe_tools WHERE recipe_id = :recipeId")
    LiveData<List<RecipeTool>> loadRecipeTools(int recipeId);

}
