package com.ifwaxtel.reciprada;

import androidx.multidex.MultiDexApplication;

import com.squareup.leakcanary.LeakCanary;

/**
 * The type M application.
 */
public class MApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }
}
