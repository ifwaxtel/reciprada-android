package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.ifwaxtel.reciprada.core.model.Ingredient;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeIngredient;
import com.ifwaxtel.reciprada.core.model.RecipeStep;
import com.ifwaxtel.reciprada.core.model.RecipeTool;
import com.ifwaxtel.reciprada.core.model.Tool;
import com.ifwaxtel.reciprada.repository.RecipesRepository;
import com.ifwaxtel.reciprada.repository.RemoteRepository;

import java.util.List;

/**
 * The type Recipe detail view model.
 */
public class RecipeDetailViewModel extends ViewModel {

    private RecipesRepository recipesRepository;

    private LiveData<List<RecipeIngredient>> ingredients;
    private LiveData<List<RecipeTool>> tools;
    private LiveData<List<RecipeStep>> steps;


    /**
     * Instantiates a new Recipe detail view model.
     *
     * @param application   the application
     * @param id            the id
     * @param token         the token
     * @param detailsLoaded the details loaded
     */
    public RecipeDetailViewModel(Application application, int id, String token, boolean detailsLoaded) {
        recipesRepository = RecipesRepository.getInstance(application);

        if(!detailsLoaded) {
            new RemoteRepository(application).loadRecipeDetailsFromApi(token, id);
        }

        ingredients = recipesRepository.loadRecipeIngredients(id);
        tools = recipesRepository.loadRecipeTools(id);
        steps = recipesRepository.loadRecipeSteps(id);
    }

    /**
     * Get recipe ingredients live data.
     *
     * @return the live data
     */
    public LiveData<List<RecipeIngredient>> getRecipeIngredients (){
        return  ingredients;
    }

    /**
     * Get recipe tools live data.
     *
     * @return the live data
     */
    public LiveData<List<RecipeTool>> getRecipeTools (){
        return  tools;
    }

    /**
     * Get recipe steps live data.
     *
     * @return the live data
     */
    public LiveData<List<RecipeStep>> getRecipeSteps (){
        return  steps;
    }

    /**
     * Update favourite.
     *
     * @param recipe the recipe
     */
    public void updateFavourite(Recipe recipe){
        recipesRepository.updateRecipe(recipe);
    }

    /**
     * Get ingredient by id ingredient.
     *
     * @param ingredientId the ingredient id
     * @return the ingredient
     */
    public Ingredient getIngredientById(int ingredientId){
        return recipesRepository.loadIngredientByid(ingredientId);
    }

    /**
     * Get tool by id tool.
     *
     * @param toolId the tool id
     * @return the tool
     */
    public Tool getToolById(int toolId){
        return recipesRepository.loadToolByid(toolId);
    }





}
