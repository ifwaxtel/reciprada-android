package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.repository.CategoriesRepository;
import com.ifwaxtel.reciprada.repository.RecipesRepository;

import java.util.List;

/**
 * The type Home view model.
 */
public class HomeViewModel extends AndroidViewModel {

    private RecipesRepository recipesRepository;

    private LiveData<List<Recipe>> recipes;
    private LiveData<List<Recipe>> favourites;
    private LiveData<List<Category>> categories;

    /**
     * Instantiates a new Home view model.
     *
     * @param application the application
     */
    public HomeViewModel(@NonNull Application application) {
        super(application);

        recipesRepository = RecipesRepository.getInstance(application);

        recipes = recipesRepository.loadRecipes();
        categories = CategoriesRepository.getInstance(application).loadHomeCategories();
        favourites = recipesRepository.loadLiveFavouriteRecipes();
    }

    /**
     * Get recipes live data.
     *
     * @return the live data
     */
    public LiveData<List<Recipe>> getRecipes(){
        return recipes;
    }

    /**
     * Get categories live data.
     *
     * @return the live data
     */
    public LiveData<List<Category>> getCategories(){
        return categories;
    }

    /**
     * Get favourites live data.
     *
     * @return the live data
     */
    public LiveData<List<Recipe>> getFavourites(){
        return favourites;
    }

    /**
     * Load recipe images list.
     *
     * @param recipeId the recipe id
     * @return the list
     */
    public List<RecipeImage> loadRecipeImages(int recipeId){
        return recipesRepository.loadRecipeImages(recipeId);
    }

    /**
     * Load recipe category category.
     *
     * @param recipeId the recipe id
     * @return the category
     */
    public Category loadRecipeCategory(int recipeId){
        return recipesRepository.loadRecipeCategory(recipeId);
    }
}
