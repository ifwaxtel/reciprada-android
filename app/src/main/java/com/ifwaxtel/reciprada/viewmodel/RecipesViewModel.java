package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.repository.CategoriesRepository;
import com.ifwaxtel.reciprada.repository.RecipesRepository;

import java.util.List;

/**
 * The type Recipes view model.
 */
public class RecipesViewModel extends AndroidViewModel {

    private RecipesRepository recipesRepository;

    private LiveData<List<Recipe>> recipes;
    private LiveData<PagedList<Recipe>> favourites;
    private LiveData<List<Category>> categories;
    private LiveData<PagedList<Recipe>> pagedListLiveData;

    /**
     * Instantiates a new Recipes view model.
     *
     * @param application the application
     */
    public RecipesViewModel(@NonNull Application application) {
        super(application);
        recipesRepository = RecipesRepository.getInstance(application);

        pagedListLiveData = recipesRepository.loadPagedRecipes();
        favourites = recipesRepository.loadFavouriteRecipes();
        categories = CategoriesRepository.getInstance(application).loadCategories();
    }

    /**
     * Get favourites live data.
     *
     * @return the live data
     */
    public LiveData<PagedList<Recipe>> getFavourites(){
        return favourites;
    }

    /**
     * Get categories live data.
     *
     * @return the live data
     */
    public LiveData<List<Category>> getCategories(){
        return categories;
    }

    /**
     * Load recipe images list.
     *
     * @param recipeId the recipe id
     * @return the list
     */
    public List<RecipeImage> loadRecipeImages(int recipeId){
        return recipesRepository.loadRecipeImages(recipeId);
    }

    /**
     * Load recipe category category.
     *
     * @param recipeId the recipe id
     * @return the category
     */
    public Category loadRecipeCategory(int recipeId){
        return recipesRepository.loadRecipeCategory(recipeId);
    }

    /**
     * Gets paged list live data.
     *
     * @return the paged list live data
     */
    public LiveData<PagedList<Recipe>> getPagedListLiveData() {
        return pagedListLiveData;
    }
}
