package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.repository.CategoriesRepository;

import java.util.List;

/**
 * The type Categories view model.
 */
public class CategoriesViewModel extends AndroidViewModel {

    private CategoriesRepository categoriesRepository;

    private LiveData<List<Category>> categories;

    /**
     * Instantiates a new Categories view model.
     *
     * @param application the application
     */
    public CategoriesViewModel(@NonNull Application application) {
        super(application);
        categoriesRepository = CategoriesRepository.getInstance(application);

        categories = categoriesRepository.loadCategories();
    }

    /**
     * Get categories live data.
     *
     * @return the live data
     */
    public LiveData<List<Category>> getCategories(){
        return categories;
    }

    /**
     * Get more categories list.
     *
     * @param lastCategoryId the last category id
     * @return the list
     */
    public List<Category> getMoreCategories(int lastCategoryId){
        return categoriesRepository.loadMoreCategories(lastCategoryId);
    }


}
