package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.PagedList;

import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.repository.RecipesRepository;

import java.util.List;

/**
 * The type Category recipes view model.
 */
public class CategoryRecipesViewModel extends ViewModel {

    private RecipesRepository recipesRepository;

    private LiveData<PagedList<Recipe>> recipes;

    /**
     * Instantiates a new Category recipes view model.
     *
     * @param application the application
     * @param id          the id
     * @param token       the token
     */
    public CategoryRecipesViewModel(Application application, int id, String token) {
        recipesRepository = RecipesRepository.getInstance(application);

        recipes = recipesRepository.loadRecipesByCategory(id);
    }

    /**
     * Gets recipes.
     *
     * @return the recipes
     */
    public LiveData<PagedList<Recipe>> getRecipes() {
        return recipes;
    }


    /**
     * Load recipe images list.
     *
     * @param recipeId the recipe id
     * @return the list
     */
    public List<RecipeImage> loadRecipeImages(int recipeId){
        return recipesRepository.loadRecipeImages(recipeId);
    }

    /**
     * Load recipe category category.
     *
     * @param recipeId the recipe id
     * @return the category
     */
    public Category loadRecipeCategory(int recipeId){
        return recipesRepository.loadRecipeCategory(recipeId);
    }

}
