package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.utils.PrefUtil;

/**
 * The type Recipe detail view model repository.
 */
public class RecipeDetailViewModelRepository extends ViewModelProvider.NewInstanceFactory{
        private final Application application;
        private final int recipeId;
        private final String token;
        private final boolean detailsLoaded;

    /**
     * Instantiates a new Recipe detail view model repository.
     *
     * @param _application   the application
     * @param id             the id
     * @param _detailsLoaded the details loaded
     */
    public RecipeDetailViewModelRepository(Application _application, int id, boolean _detailsLoaded) {
            this.application = _application;
            this.recipeId = id;
            this.token = "Bearer " + new PrefUtil(_application).getToken();
            this.detailsLoaded = _detailsLoaded;

        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

            return (T) new RecipeDetailViewModel(application, recipeId, token, detailsLoaded);
        }
}
