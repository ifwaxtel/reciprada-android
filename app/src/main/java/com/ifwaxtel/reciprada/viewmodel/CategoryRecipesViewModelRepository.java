package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.utils.PrefUtil;

/**
 * The type Category recipes view model repository.
 */
public class CategoryRecipesViewModelRepository extends ViewModelProvider.NewInstanceFactory{
        private final Application application;
        private final int categoryId;
        private final String token;

    /**
     * Instantiates a new Category recipes view model repository.
     *
     * @param _application the application
     * @param id           the id
     */
    public CategoryRecipesViewModelRepository(Application _application, int id) {
            this.application = _application;
            this.categoryId = id;
            this.token = "Bearer " + new PrefUtil(_application).getToken();
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

            return (T) new CategoryRecipesViewModel(application, categoryId, token);
        }
}
