package com.ifwaxtel.reciprada.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.repository.RemoteRepository;
import com.ifwaxtel.reciprada.utils.PrefUtil;

/**
 * The type Main view model.
 */
public class MainViewModel extends AndroidViewModel{

    private PrefUtil prefUtil;

    /**
     * Instantiates a new Main view model.
     *
     * @param application the application
     */
    public MainViewModel(@NonNull Application application) {
        super(application);
        prefUtil = new PrefUtil(application.getApplicationContext());
    }

    /**
     * Get token.
     *
     * @param listener the listener
     */
    public void getToken(RepositoryListener.TokenRetrieve listener){
        String token = prefUtil.getToken();
        if(token != null){
            listener.onFetchTokenSuccess(token);
        }
        else{
            RemoteRepository.fetchToken(prefUtil,listener);
        }
    }

}
