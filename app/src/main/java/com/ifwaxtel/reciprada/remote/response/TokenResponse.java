package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

/**
 * The type Token response.
 */
public class TokenResponse {
    @SerializedName(GlobalConstants.STATUS)
    @Expose
    private String status;

    @SerializedName(GlobalConstants.MESSAGE)
    @Expose
    private String message;

    @SerializedName(GlobalConstants.TOKEN)
    @Expose
    private String token;

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }
}
