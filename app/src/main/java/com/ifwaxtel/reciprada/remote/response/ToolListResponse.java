package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Tool;

import java.util.ArrayList;

/**
 * The type Tool list response.
 */
public class ToolListResponse {
    @SerializedName(GlobalConstants.DATA)
    @Expose
    private ArrayList<Tool> tools;

    /**
     * Gets tools.
     *
     * @return the tools
     */
    public ArrayList<Tool> getTools() {
        return tools;
    }

    /**
     * Sets tools.
     *
     * @param tools the tools
     */
    public void setTools(ArrayList<Tool> tools) {
        this.tools = tools;
    }
}
