package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Recipe;

/**
 * The type Recipe details response.
 */
public class RecipeDetailsResponse {
    @SerializedName(GlobalConstants.DATA)
    @Expose
    private Recipe recipe;

    /**
     * Gets recipe.
     *
     * @return the recipe
     */
    public Recipe getRecipe() {
        return recipe;
    }

    /**
     * Sets recipe.
     *
     * @param recipe the recipe
     */
    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
