package com.ifwaxtel.reciprada.remote.request;

import com.ifwaxtel.reciprada.remote.response.CategoryListResponse;
import com.ifwaxtel.reciprada.remote.response.IngredientListResponse;
import com.ifwaxtel.reciprada.remote.response.RecipeDetailsResponse;
import com.ifwaxtel.reciprada.remote.response.RecipeListResponse;
import com.ifwaxtel.reciprada.remote.response.TokenResponse;
import com.ifwaxtel.reciprada.remote.response.ToolListResponse;
import com.ifwaxtel.reciprada.remote.response.UserResponse;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * The interface Api service.
 */
public interface ApiService {


    /**
     * Gets api token.
     *
     * @param api_key the api key
     * @return the api token
     */
    @POST("token/generate")
    @FormUrlEncoded
    Call<TokenResponse> getApiToken(@Field(GlobalConstants.API_KEY) String api_key);


    /**
     * Register call.
     *
     * @param firstName   the first name
     * @param lastName    the last name
     * @param countryCode the country code
     * @param phoneNumber the phone number
     * @param email       the email
     * @param password    the password
     * @return the call
     */
    @POST("user/register")
    @FormUrlEncoded
    Call<UserResponse> register(@Field(GlobalConstants.FIRST_NAME) String firstName,
                                @Field(GlobalConstants.LAST_NAME) String lastName,
                                @Field(GlobalConstants.COUNTRY_CODE) String countryCode,
                                @Field(GlobalConstants.PHONE_NUMBER) String phoneNumber,
                                @Field(GlobalConstants.EMAIL) String email,
                                @Field(GlobalConstants.PASSWORD) String password);

    /**
     * Register social call.
     *
     * @param firstName   the first name
     * @param lastName    the last name
     * @param countryCode the country code
     * @param phoneNumber the phone number
     * @param email       the email
     * @param password    the password
     * @return the call
     */
    @POST("user/register_social")
    @FormUrlEncoded
    Call<UserResponse> registerSocial(@Field(GlobalConstants.FIRST_NAME) String firstName,
                                @Field(GlobalConstants.LAST_NAME) String lastName,
                                @Field(GlobalConstants.COUNTRY_CODE) String countryCode,
                                @Field(GlobalConstants.PHONE_NUMBER) String phoneNumber,
                                @Field(GlobalConstants.EMAIL) String email,
                                @Field(GlobalConstants.PASSWORD) String password);

    /**
     * Upload profile picture call.
     *
     * @param profile_picture the profile picture
     * @return the call
     */
    @Multipart
    @POST("user/upload/profile_picture")
    Call<UserResponse> uploadProfilePicture(@Part MultipartBody.Part profile_picture);

    /**
     * Login call.
     *
     * @param email    the email
     * @param password the password
     * @return the call
     */
    @POST("user/login")
    @FormUrlEncoded
    Call<UserResponse> login(@Field(GlobalConstants.EMAIL) String email,
                             @Field(GlobalConstants.PASSWORD) String password);

    /**
     * Load categories call.
     *
     * @param request_token the request token
     * @param pageNo        the page no
     * @return the call
     */
    @GET("categories/all")
    Call<CategoryListResponse> loadCategories(@Header("Authorization") String request_token, @Query("page") int pageNo);

    /**
     * Load ingredients call.
     *
     * @param request_token the request token
     * @return the call
     */
    @GET("ingredients")
    Call<IngredientListResponse> loadIngredients(@Header("Authorization") String request_token);

    /**
     * Load tools call.
     *
     * @param request_token the request token
     * @return the call
     */
    @GET("tools")
    Call<ToolListResponse> loadTools(@Header("Authorization") String request_token);

    /**
     * Load recipes call.
     *
     * @param request_token the request token
     * @param pageNo        the page no
     * @return the call
     */
    @GET("recipes")
    Call<RecipeListResponse> loadRecipes(@Header("Authorization") String request_token, @Query("page") int pageNo);

    /**
     * Load recipes by category call.
     *
     * @param request_token the request token
     * @param categoryId    the category id
     * @param pageNo        the page no
     * @return the call
     */
    @GET("recipes/category/{id}")
    Call<RecipeListResponse> loadRecipesByCategory(@Header("Authorization") String request_token, @Path("id") int categoryId, @Query("page") int pageNo);

    /**
     * Load recipes recommended call.
     *
     * @param request_token the request token
     * @param pageNo        the page no
     * @return the call
     */
    @GET("recipes/recommended")
    Call<RecipeListResponse> loadRecipesRecommended(@Header("Authorization") String request_token, @Query("page") int pageNo);

    /**
     * Load recipe details call.
     *
     * @param request_token the request token
     * @param recipeId      the recipe id
     * @return the call
     */
    @GET("recipes/{id}")
    Call<RecipeDetailsResponse> loadRecipeDetails(@Header("Authorization") String request_token, @Path("id") int recipeId);

}
