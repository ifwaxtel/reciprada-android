package com.ifwaxtel.reciprada.remote.request;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The type Api client.
 */
public class ApiClient {
    private static Retrofit retrofit = null;

    /**
     * Get client retrofit.
     *
     * @return the retrofit
     */
    public static Retrofit getClient(){
        if(retrofit == null){
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(600, TimeUnit.SECONDS)
                    .connectTimeout(600, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .addInterceptor(
                            chain -> {
                                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                                return chain.proceed(request);
                            }
                    ).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(GlobalConstants.BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
