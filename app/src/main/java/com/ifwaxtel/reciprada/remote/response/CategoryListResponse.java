package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Category;

import java.util.ArrayList;

/**
 * The type Category list response.
 */
public class CategoryListResponse {
    @SerializedName(GlobalConstants.DATA)
    @Expose
    private ArrayList<Category> categories;

    /**
     * Gets categories.
     *
     * @return the categories
     */
    public ArrayList<Category> getCategories() {
        return categories;
    }

    /**
     * Sets categories.
     *
     * @param categories the categories
     */
    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
