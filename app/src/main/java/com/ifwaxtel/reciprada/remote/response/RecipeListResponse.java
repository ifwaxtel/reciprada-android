package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Recipe;

import java.util.ArrayList;

/**
 * The type Recipe list response.
 */
public class RecipeListResponse {

    @SerializedName(GlobalConstants.DATA)
    @Expose
    private ArrayList<Recipe> recipes;

    /**
     * Gets recipes.
     *
     * @return the recipes
     */
    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    /**
     * Sets recipes.
     *
     * @param recipes the recipes
     */
    public void setRecipes(ArrayList<Recipe> recipes) {
        this.recipes = recipes;
    }
}
