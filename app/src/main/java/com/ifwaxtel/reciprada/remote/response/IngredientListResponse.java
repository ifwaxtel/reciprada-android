package com.ifwaxtel.reciprada.remote.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.Ingredient;

import java.util.ArrayList;

/**
 * The type Ingredient list response.
 */
public class IngredientListResponse {
    @SerializedName(GlobalConstants.DATA)
    @Expose
    private ArrayList<Ingredient> ingrediets;


    /**
     * Gets ingrediets.
     *
     * @return the ingrediets
     */
    public ArrayList<Ingredient> getIngrediets() {
        return ingrediets;
    }

    /**
     * Sets ingrediets.
     *
     * @param ingrediets the ingrediets
     */
    public void setIngrediets(ArrayList<Ingredient> ingrediets) {
        this.ingrediets = ingrediets;
    }
}
