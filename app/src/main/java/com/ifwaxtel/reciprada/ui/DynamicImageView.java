package com.ifwaxtel.reciprada.ui;

import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;

/**
 * The type Dynamic image view.
 */
public class DynamicImageView extends AppCompatImageView {
    private float mAspectRatio = 1.5f;

    /**
     * Instantiates a new Dynamic image view.
     *
     * @param context the context
     */
    public DynamicImageView(Context context) {
        super(context);
    }

    /**
     * Sets aspect ratio.
     *
     * @param aspectRatio the aspect ratio
     */
    public void setAspectRatio(float aspectRatio) {
        mAspectRatio = aspectRatio;
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int measuredWidth = getMeasuredWidth();
        setMeasuredDimension(measuredWidth, (int) (measuredWidth / mAspectRatio));
    }

}
