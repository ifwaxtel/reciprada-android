package com.ifwaxtel.reciprada.fragment.auth;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.SlidePagerAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentLandingBinding;
import com.ifwaxtel.reciprada.listeners.AuthListener;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Landing fragment.
 */
public class LandingFragment extends Fragment implements View.OnClickListener{
    /**
     * The constant TAG.
     */
    public static final String TAG = LandingFragment.class.getSimpleName();

    private AuthListener.OnFragmentInteractionListener mListener;

    private FragmentLandingBinding binding;

    /**
     * Instantiates a new Landing fragment.
     */
    public LandingFragment() {}

    /**
     * New instance landing fragment.
     *
     * @return the landing fragment
     */
    public static LandingFragment newInstance() {
        return new LandingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_landing, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /***
     * Make sure the parent fragment implements the @AuthListener.OnFragmentInteractionListener
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AuthListener.OnFragmentInteractionListener) {
            mListener = (AuthListener.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /***
     * Setup the default listeners and add data to the current view
     */
    private void init(){
        SlidePagerAdapter adapter = new SlidePagerAdapter();
        adapter.setData(createPageList());

        binding.viewPager.setAdapter(adapter);

        binding.skipButton.setOnClickListener(this);
        binding.login.setOnClickListener(this);
        binding.signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mListener.onFragmentInteraction(v);
    }

    /***
     * Setup the slider in the landing activity
     * This uses the @pageindicatorview library
     */
    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();
        pageList.add(createPageView(R.drawable.bg_3));
        pageList.add(createPageView(R.drawable.bg_1));
        pageList.add(createPageView(R.drawable.bg_2));
        pageList.add(createPageView(R.drawable.bg_4));
        pageList.add(createPageView(R.drawable.bg_5));

        return pageList;
    }

    /***
     * Create an imageView item for the pageindicatorview slider
     */
    @NonNull
    private ImageView createPageView(int image) {
        ImageView view = new ImageView(getContext());
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setImageDrawable(getResources().getDrawable(image));

        return view;
    }
}
