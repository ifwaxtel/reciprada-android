package com.ifwaxtel.reciprada.fragment.auth;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.FragmentLoginBinding;
import com.ifwaxtel.reciprada.listeners.AuthListener;
import com.ifwaxtel.reciprada.utils.AuthUtil;
import com.ifwaxtel.reciprada.utils.DialogUtil;
import com.ifwaxtel.reciprada.utils.InputUtil;

import java.util.Objects;

/**
 * The type Login fragment.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {

    /**
     * The constant TAG.
     */
    public static final String TAG = LoginFragment.class.getSimpleName();

    private AuthListener.OnFragmentInteractionListener mListener;
    private AuthListener.RemoteResponseInterface remoteListener;
    private DialogUtil dialogUtil;

    private FragmentLoginBinding binding;

    /**
     * Instantiates a new Login fragment.
     */
    public LoginFragment() {}

    /**
     * New instance login fragment.
     *
     * @param _remoteListener the remote listener
     * @param _dialogUtil     the dialog util
     * @return the login fragment
     */
    public static LoginFragment newInstance(AuthListener.RemoteResponseInterface _remoteListener, DialogUtil _dialogUtil) {
        LoginFragment loginFragment =  new LoginFragment();
        loginFragment.remoteListener = _remoteListener;
        loginFragment.dialogUtil = _dialogUtil;
        return loginFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /***
     * Setup the default listeners and add data to the current view
     */
    private void init(){
        binding.passwordTextInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        binding.passwordTextInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
        binding.skipButton.setOnClickListener(this);
        binding.backButton.setOnClickListener(this);
        binding.login.setOnClickListener(this);
        binding.signup.setOnClickListener(this);
        binding.signupWithFacebook.setOnClickListener(this);
        binding.signupWithGoogle.setOnClickListener(this);

        InputUtil.resetError(binding.emailTextInput,binding.emailInputLayout);
        InputUtil.resetError(binding.passwordTextInput,binding.passwordInputLayout);
    }
    /***
     * Make sure the parent fragment implements the @AuthListener.OnFragmentInteractionListener
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AuthListener.OnFragmentInteractionListener) {
            mListener = (AuthListener.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.login){
            processLoginData(Objects.requireNonNull(binding.emailTextInput.getText()).toString().trim(),
                    Objects.requireNonNull(binding.passwordTextInput.getText()).toString().trim());
        }
        else {
            mListener.onFragmentInteraction(v);
        }
    }

    /***
     * Check if the entered data is valid and send the data to the web client for validation
     * if the data is valid then store the returned user object as a preferences
     * @param email the users email
     * @param password the password entered
     */
    private void processLoginData(String email, String password){
        if(validateData(email,password)){

            dialogUtil.getFixedDialog(getActivity(),"Logging in..").show();

            AuthUtil authUtil = new AuthUtil(remoteListener);
            authUtil.login(email,password);
        }
    }

    /***
     * Validate that the values entered are valid
     * @param email the entered email
     * @param password the entered password
     * @return boolean (true or false)
     */
    private boolean validateData(String email, String password){
        if(!InputUtil.isValidEmail(email)){
            binding.emailInputLayout.setError("Invalid email address");
            return false;
        }
        if(!InputUtil.isValidLength(password,6)){
            binding.passwordInputLayout.setError("Password must have at least 6 characters");
            return false;
        }
        return true;
    }
}
