package com.ifwaxtel.reciprada.fragment.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.CategoryAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentCategoriesBinding;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.utils.GridSpanSizeLookup;
import com.ifwaxtel.reciprada.viewmodel.CategoriesViewModel;

import java.util.List;
import java.util.Objects;


/**
 * The type Categories fragment.
 */
public class CategoriesFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {
    /**
     * The constant TAG.
     */
    public static final String TAG = CategoriesFragment.class.getSimpleName();

    private MainListener.OnFragmentInteractionListener mListener;
    private CategoryAdapter.RecyclerClickListener categoryListener;

    private FragmentCategoriesBinding binding;
    private CategoryAdapter categoryAdapter;
    private CategoriesViewModel categoriesViewModel;

    /**
     * Instantiates a new Categories fragment.
     */
    public CategoriesFragment() {}

    /**
     * New instance categories fragment.
     *
     * @return the categories fragment
     */
    public static CategoriesFragment newInstance() {
        return new CategoriesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        categoryAdapter = new CategoryAdapter(getActivity(), categoryListener);
        categoriesViewModel = ViewModelProviders.of(this).get(CategoriesViewModel.class);
        categoriesViewModel.getCategories().observe(this, categoriesObserver);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    private void init() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(binding.toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), binding.drawerLayout, binding.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);

        binding.navView.setCheckedItem(R.id.nav_categories);

        binding.categoryRecyclerView.setNestedScrollingEnabled(false);

        setOffset();

        setUpCategories();
    }

    private void setOffset() {
        binding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    binding.collapsingAppBar.setTitle("Categories");
                    isShow = true;
                } else if (isShow) {
                    binding.collapsingAppBar.setTitle(" ");
                    isShow = false;
                }

            }
        });
    }

    private void setUpCategories() {
        binding.categoryRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridSpanSizeLookup(5, 1, 2));
        binding.categoryRecyclerView.setLayoutManager(gridLayoutManager);
        binding.categoryRecyclerView.setAdapter(categoryAdapter);

        binding.categoryRecyclerView.addOnScrollListener(categoryScrollListener);
    }

    private Observer<List<Category>> categoriesObserver = new Observer<List<Category>>() {
        @Override
        public void onChanged(@Nullable List<Category> categories) {
            categoryAdapter.addCategories(categories);
        }
    };

    private RecyclerView.OnScrollListener categoryScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if (!recyclerView.canScrollVertically(1)) {
                binding.loadingProgress.setVisibility(View.VISIBLE);

                categoryAdapter.addMoreRecipes(categoriesViewModel.getMoreCategories(categoryAdapter.getLastItemId()));
                binding.loadingProgress.setVisibility(View.GONE);

                Log.w("Recommended", "Scrolled to end");
            }
            else{
                binding.loadingProgress.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        binding.navView.setCheckedItem(R.id.nav_categories);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainListener.OnFragmentInteractionListener) {
            mListener = (MainListener.OnFragmentInteractionListener) context;
            categoryListener = (CategoryAdapter.RecyclerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        categoryListener = null;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.home, menu);
    }

    /**
     * The interface On fragment interaction listener.
     */
    public interface OnFragmentInteractionListener {
        /**
         * On fragment interaction.
         *
         * @param uri the uri
         */
// TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() != R.id.nav_categories)
            mListener.onFragmentInteraction(item.getItemId());

        binding.drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

}
