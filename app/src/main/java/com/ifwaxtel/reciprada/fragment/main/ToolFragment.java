package com.ifwaxtel.reciprada.fragment.main;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.FragmentToolBinding;
import com.ifwaxtel.reciprada.core.model.Tool;

import java.util.Objects;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * A simple {@link Fragment} subclass.
 */
public class ToolFragment extends Fragment implements View.OnClickListener {
    /**
     * The constant TAG.
     */
    public static final String TAG = ToolFragment.class.getSimpleName();
    private static final String TOOL_OBJECT = "tool_object";

    private FragmentToolBinding binding;
    private Tool tool;

    /**
     * Instantiates a new Tool fragment.
     */
    public ToolFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tool = getArguments().getParcelable(TOOL_OBJECT);
        }
    }

    /**
     * New instance tool fragment.
     *
     * @param item the item
     * @return the tool fragment
     */
    public static ToolFragment newInstance(Tool item) {
        ToolFragment fragment = new ToolFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(TOOL_OBJECT, item);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tool, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.toolTitle.setText(tool.getName());
        binding.toolDescription.setText(tool.getDescription());
        GlideApp.with(Objects.requireNonNull(getContext()))
                .load(tool.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(withCrossFade())
                .into(binding.toolImage);
        binding.closeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_button:
                Objects.requireNonNull(getFragmentManager()).popBackStack();
                break;
        }
    }
}
