package com.ifwaxtel.reciprada.fragment.auth;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.FragmentSignupMethodBinding;
import com.ifwaxtel.reciprada.listeners.AuthListener;

/**
 * The type Signup method fragment.
 */
public class SignupMethodFragment extends Fragment implements View.OnClickListener {

    /**
     * The constant TAG.
     */
    public static final String TAG = SignupMethodFragment.class.getSimpleName();

    private AuthListener.OnFragmentInteractionListener mListener;

    private FragmentSignupMethodBinding binding;

    /**
     * Instantiates a new Signup method fragment.
     */
    public SignupMethodFragment() {
        // Required empty public constructor
    }

    /**
     * New instance signup method fragment.
     *
     * @return the signup method fragment
     */
    public static SignupMethodFragment newInstance() {
        return new SignupMethodFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup_method, container, false);
        return  binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    /***
     * Make sure the parent fragment implements the @AuthListener.OnFragmentInteractionListener
     */
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AuthListener.OnFragmentInteractionListener) {
            mListener = (AuthListener.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /***
     * Setup the default listeners and add data to the current view
     */
    private void init(){
        binding.skipButton.setOnClickListener(this);
        binding.login.setOnClickListener(this);
        binding.signupWithEmail.setOnClickListener(this);
        binding.signupWithGoogle.setOnClickListener(this);
        binding.signupWithFacebook.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mListener.onFragmentInteraction(v);
    }

}
