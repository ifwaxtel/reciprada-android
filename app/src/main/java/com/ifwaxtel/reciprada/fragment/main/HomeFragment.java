package com.ifwaxtel.reciprada.fragment.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.CategoryAdapter;
import com.ifwaxtel.reciprada.adapter.RecipeAdapter;
import com.ifwaxtel.reciprada.adapter.SlidePagerAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentHomeBinding;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.utils.GridSpanSizeLookup;
import com.ifwaxtel.reciprada.viewmodel.HomeViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * The type Home fragment.
 */
public class HomeFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The constant TAG.
     */
    public static final String TAG = HomeFragment.class.getSimpleName();

    private MainListener.OnFragmentInteractionListener mListener;
    private RecipeAdapter.RecyclerClickListener recipeListener;
    private CategoryAdapter.RecyclerClickListener categoryListener;

    private FragmentHomeBinding binding;
    private RecipeAdapter recipeAdapter;
    private RecipeAdapter favAdapter;

    private CategoryAdapter categoryAdapter;
    private HomeViewModel homeViewModel;
    private ExecutorService executorService;

    /**
     * Instantiates a new Home fragment.
     */
    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * New instance home fragment.
     *
     * @return the home fragment
     */
    public static HomeFragment newInstance() {

        HomeFragment fragment = new HomeFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        executorService = Executors.newSingleThreadExecutor();

        recipeAdapter = new RecipeAdapter(getActivity(), recipeListener);
        favAdapter = new RecipeAdapter(getActivity(), recipeListener);

        categoryAdapter = new CategoryAdapter(getActivity(), categoryListener);
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        homeViewModel.getCategories().observe(this, categoriesObserver);

        homeViewModel.getRecipes().observe(this, recipes -> {
            if (recipes != null) {

                for (Recipe recipe : Objects.requireNonNull(recipes)) {
                    Callable<List<RecipeImage>> imageCallable = () -> homeViewModel.loadRecipeImages(recipe.getId());
                    Callable<Category> categoryCallable = () -> homeViewModel.loadRecipeCategory(recipe.getId());
                    try {
                        recipe.setRecipeImages(executorService.submit(imageCallable).get());
                        recipe.setCategory(executorService.submit(categoryCallable).get());
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    } catch (ExecutionException e) {
                        //e.printStackTrace();
                    }
                }
                Log.w("HomeFragment", recipes.toString());
                showEmptyList(recipes.size() == 0, binding.homeContent.recommended.emptyView, binding.homeContent.recommended.recyclerView);

                recipeAdapter.addRecipes(recipes);
            }
        });

        homeViewModel.getFavourites().observe(this, recipes -> {
            if (recipes != null) {
                for (Recipe recipe : Objects.requireNonNull(recipes)) {
                    Callable<List<RecipeImage>> imageCallable = () -> homeViewModel.loadRecipeImages(recipe.getId());
                    Callable<Category> categoryCallable = () -> homeViewModel.loadRecipeCategory(recipe.getId());
                    try {
                        recipe.setRecipeImages(executorService.submit(imageCallable).get());
                        recipe.setCategory(executorService.submit(categoryCallable).get());
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    } catch (ExecutionException e) {
                        //e.printStackTrace();
                    }
                }
                showEmptyList(recipes.size() == 0, binding.homeContent.favourite.emptyView, binding.homeContent.favourite.recyclerView);
                favAdapter.addRecipes(recipes);
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void showEmptyList(boolean show, View emptyView, View recyclerView) {
        if (show) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.navView.setCheckedItem(R.id.nav_home);

        if (getArguments() != null) {
            if (getArguments().getParcelable("recommendedState") != null) {
                Objects.requireNonNull(binding.homeContent.recommended.recyclerView.getLayoutManager()).onRestoreInstanceState(getArguments().getParcelable("recommendedState"));
            }

            if (getArguments().getParcelable("favouriteState") != null) {
                Objects.requireNonNull(binding.homeContent.favourite.recyclerView.getLayoutManager()).onRestoreInstanceState(getArguments().getParcelable("favouriteState"));
            }
        }
        Log.w("onResume", "running..");
    }

    @Override
    public void onPause() {
        super.onPause();
        // Save state
        Parcelable recommendedState = Objects.requireNonNull(binding.homeContent.recommended.recyclerView.getLayoutManager()).onSaveInstanceState();
        Parcelable favouriteState = Objects.requireNonNull(binding.homeContent.favourite.recyclerView.getLayoutManager()).onSaveInstanceState();
        Objects.requireNonNull(getArguments()).putParcelable("recommendedState", recommendedState);
        getArguments().putParcelable("favouriteState", favouriteState);
        Log.w("onPause", "running..");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainListener.OnFragmentInteractionListener) {
            mListener = (MainListener.OnFragmentInteractionListener) context;
            recipeListener = (RecipeAdapter.RecyclerClickListener) context;
            categoryListener = (CategoryAdapter.RecyclerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        Log.w("onAttach", "running..");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        recipeListener = null;
        categoryListener = null;

        Log.w("onDetach", "running..");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.home, menu);
    }


    private void init() {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(binding.toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), binding.drawerLayout, binding.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);

        binding.navView.setCheckedItem(R.id.nav_home);

        setOffset();

        SlidePagerAdapter adapter = new SlidePagerAdapter();
        adapter.setData(createPageList());

        binding.viewPager.setAdapter(adapter);

        binding.homeContent.recommended.recyclerView.setNestedScrollingEnabled(false);
        binding.homeContent.favourite.recyclerView.setNestedScrollingEnabled(false);
        binding.homeContent.categories.recyclerView.setNestedScrollingEnabled(false);

        setupPopularRecipes();
    }

    private void setOffset() {
        binding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    binding.collapsingAppBar.setTitle(getResources().getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    binding.collapsingAppBar.setTitle(" ");
                    isShow = false;
                }

            }
        });
    }

    /**
     * Gets listener.
     *
     * @return the listener
     */
    public MainListener.OnFragmentInteractionListener getmListener() {
        return mListener;
    }
    private void setupPopularRecipes() {
        binding.homeContent.recommended.recyclerView.setHasFixedSize(true);
        binding.homeContent.recommended.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.homeContent.recommended.recyclerView.setAdapter(recipeAdapter);


        binding.homeContent.categories.recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridSpanSizeLookup(5, 1, 2));
        binding.homeContent.categories.recyclerView.setLayoutManager(gridLayoutManager);
        binding.homeContent.categories.recyclerView.setAdapter(categoryAdapter);
        categoryAdapter.setEmptyView(binding.homeContent.categories.emptyView);

        binding.homeContent.favourite.recyclerView.setHasFixedSize(true);
        binding.homeContent.favourite.recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.homeContent.favourite.recyclerView.setAdapter(favAdapter);
    }

    private Observer<List<Category>> categoriesObserver = new Observer<List<Category>>() {
        @Override
        public void onChanged(@Nullable List<Category> categories) {
            categoryAdapter.addCategories(categories);
        }
    };

    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();
        pageList.add(createPageView(R.drawable.bg_1));
        pageList.add(createPageView(R.drawable.bg_2));

        return pageList;
    }

    @NonNull
    private ImageView createPageView(int image) {
        ImageView view = new ImageView(getContext());
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setImageDrawable(getResources().getDrawable(image));

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.action_search) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.

        if (menuItem.getItemId() != R.id.nav_home) {
            mListener.onFragmentInteraction(menuItem.getItemId());
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}

