package com.ifwaxtel.reciprada.fragment.auth;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.FragmentSignupWithEmailBinding;
import com.ifwaxtel.reciprada.listeners.AuthListener;
import com.ifwaxtel.reciprada.utils.AuthUtil;
import com.ifwaxtel.reciprada.utils.DialogUtil;
import com.ifwaxtel.reciprada.utils.InputUtil;

import java.util.Objects;


/**
 * The type Signup with email fragment.
 */
public class SignupWithEmailFragment extends Fragment implements View.OnClickListener{
    /**
     * The constant TAG.
     */
    public static final String TAG = SignupWithEmailFragment.class.getSimpleName();

    private AuthListener.OnFragmentInteractionListener mListener;
    private AuthListener.RemoteResponseInterface remoteListener;
    private DialogUtil dialogUtil;

    private FragmentSignupWithEmailBinding binding;

    /**
     * Instantiates a new Signup with email fragment.
     */
    public SignupWithEmailFragment() {
        // Required empty public constructor
    }

    /**
     * New instance signup with email fragment.
     *
     * @param _remoteListener the remote listener
     * @param _dialogUtil     the dialog util
     * @return the signup with email fragment
     */
    public static SignupWithEmailFragment newInstance(AuthListener.RemoteResponseInterface _remoteListener, DialogUtil _dialogUtil) {
        SignupWithEmailFragment fragment = new SignupWithEmailFragment();
        fragment.remoteListener = _remoteListener;
        fragment.dialogUtil = _dialogUtil;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_signup_with_email, container, false);
        return  binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof AuthListener.OnFragmentInteractionListener) {
            mListener = (AuthListener.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void init(){
        binding.passwordTextInput.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        binding.passwordTextInput.setTransformationMethod(PasswordTransformationMethod.getInstance());
        binding.skipButton.setOnClickListener(this);
        binding.backButton.setOnClickListener(this);
        binding.signup.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.signup){
            processSignupData(
                    Objects.requireNonNull(binding.firstnameTextInput.getText()).toString().trim(),
                    Objects.requireNonNull(binding.lastnameTextInput.getText()).toString().trim(),
                    binding.countryCodeInput.getText().toString().trim(),
                    Objects.requireNonNull(binding.phoneTextInput.getText()).toString().trim(),
                    Objects.requireNonNull(binding.emailTextInput.getText()).toString().trim(),
                    Objects.requireNonNull(binding.passwordTextInput.getText()).toString().trim());
        }
        else{
            mListener.onFragmentInteraction(v);
        }
    }

    private void processSignupData(String firstName, String lastName, String countryCode,
                                   String phone, String email, String password) {
        if(validateData(firstName,lastName,countryCode,phone,email,password)){

            dialogUtil.getFixedDialog(getActivity(),"Creating account..").show();

            AuthUtil authUtil = new AuthUtil(remoteListener);
            authUtil.register(firstName, lastName, countryCode, phone, email, password);

        }

    }

    private boolean validateData(String firstName, String lastName, String countryCode,
                                 String phone, String email, String password){
        if(!InputUtil.isValidLength(firstName, 3)){
            binding.firstnameInputLayout.setError("Invalid first name");
            return false;
        }
        if(!InputUtil.isValidLength(lastName, 3)){
            binding.lastnameInputLayout.setError("Invalid last name");
            return false;
        }
        if(!InputUtil.isValidLength(countryCode, 3)){
            binding.countryCodeLayout.setError("Invalid country code");
            return false;
        }
        if(!InputUtil.isValidMobile(phone)){
            binding.phoneInputLayout.setError("Invalid phone number");
            return false;
        }
        if(!InputUtil.isValidEmail(email)){
            binding.emailInputLayout.setError("Invalid email address");
            return false;
        }
        if(!InputUtil.isValidLength(password,6)){
            binding.passwordInputLayout.setError("Password must have at least 6 characters");
            return false;
        }
        return true;
    }


}
