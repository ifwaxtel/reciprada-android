package com.ifwaxtel.reciprada.fragment.main;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.RecipePagedAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentFavouritesBinding;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.utils.GridSpanSizeLookup;
import com.ifwaxtel.reciprada.viewmodel.RecipesViewModel;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The type Favourites fragment.
 */
public class FavouritesFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener {
    /**
     * The constant TAG.
     */
    public static final String TAG = FavouritesFragment.class.getSimpleName();

    private MainListener.OnFragmentInteractionListener mListener;
    private RecipePagedAdapter.RecyclerClickListener recipeListener;

    private FragmentFavouritesBinding binding;
    private RecipePagedAdapter recipeAdapter;
    private RecipesViewModel recipesViewModel;
    private ExecutorService executorService;

    /**
     * Instantiates a new Favourites fragment.
     */
    public FavouritesFragment() {
        // Required empty public constructor
    }

    /**
     * New instance favourites fragment.
     *
     * @return the favourites fragment
     */
    public static FavouritesFragment newInstance() {
        return new FavouritesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        executorService = Executors.newSingleThreadExecutor();
        recipeAdapter = new RecipePagedAdapter(getActivity(), false, recipeListener);
        recipesViewModel = ViewModelProviders.of(this).get(RecipesViewModel.class);
        recipesViewModel.getFavourites().observe(this, recipes -> {
            if (recipes != null) {
                for (Recipe recipe : Objects.requireNonNull(recipes)) {
                    Callable<List<RecipeImage>> imageCallable = () -> recipesViewModel.loadRecipeImages(recipe.getId());
                    Callable<Category> categoryCallable = () -> recipesViewModel.loadRecipeCategory(recipe.getId());
                    try {
                        recipe.setRecipeImages(executorService.submit(imageCallable).get());
                        recipe.setCategory(executorService.submit(categoryCallable).get());
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    } catch (ExecutionException e) {
                        //e.printStackTrace();
                    }
                }
                recipeAdapter.submitList(recipes);
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favourites, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        ((AppCompatActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(binding.toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), binding.drawerLayout, binding.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navView.setNavigationItemSelectedListener(this);

        binding.navView.setCheckedItem(R.id.nav_favourites);

        setOffset();

        setupRecipes();
    }

    private void setOffset() {
        binding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    binding.collapsingAppBar.setTitle("Favourite Recipes");
                    isShow = true;
                } else if (isShow) {
                    binding.collapsingAppBar.setTitle(" ");
                    isShow = false;
                }

            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainListener.OnFragmentInteractionListener) {
            mListener = (MainListener.OnFragmentInteractionListener) context;
            recipeListener = (RecipePagedAdapter.RecyclerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        recipeListener = null;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.home, menu);
    }

    private void setupRecipes() {
        binding.recipesRecyclerView.setNestedScrollingEnabled(true);
        binding.recipesRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridSpanSizeLookup(5, 1, 2));
        binding.recipesRecyclerView.setLayoutManager(gridLayoutManager);
        //binding.homeContent.categoryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        binding.recipesRecyclerView.setAdapter(recipeAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //noinspection SimplifiableIfStatement
        if (item.getItemId() == R.id.action_search) {
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        // Handle navigation view item clicks here.

        if (menuItem.getItemId() != R.id.nav_favourites ) {
            mListener.onFragmentInteraction(menuItem.getItemId());
        }

        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

}
