package com.ifwaxtel.reciprada.fragment.main;


import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.FragmentIngredientBinding;
import com.ifwaxtel.reciprada.core.model.Ingredient;

import java.util.Objects;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * A simple {@link Fragment} subclass.
 */
public class IngredientFragment extends Fragment implements View.OnClickListener {
    /**
     * The constant TAG.
     */
    public static final String TAG = IngredientFragment.class.getSimpleName();
    private static final String INGREDIENT_OBJECT = "ingredient_object";

    private FragmentIngredientBinding binding;
    private Ingredient ingredient;

    /**
     * Instantiates a new Ingredient fragment.
     */
    public IngredientFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ingredient = getArguments().getParcelable(INGREDIENT_OBJECT);
        }
    }

    /**
     * New instance ingredient fragment.
     *
     * @param item the item
     * @return the ingredient fragment
     */
    public static IngredientFragment newInstance(Ingredient item) {
        IngredientFragment fragment = new IngredientFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(INGREDIENT_OBJECT, item);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ingredient, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.ingredientTitle.setText(ingredient.getName());
        binding.ingredientDescription.setText(ingredient.getDescription());
        binding.calories.setText(String.valueOf(ingredient.getCalorieCount()));
        binding.carbohydrate.setText(String.valueOf(ingredient.getCarbohydrateCount()));
        binding.protein.setText(String.valueOf(ingredient.getProteinCount()));

        GlideApp.with(Objects.requireNonNull(getContext()))
                .load(ingredient.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(withCrossFade())
                .into(binding.ingredientImage);

        binding.closeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.close_button:
                Objects.requireNonNull(getFragmentManager()).popBackStack();
                break;
        }
    }
}
