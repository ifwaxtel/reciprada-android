package com.ifwaxtel.reciprada.fragment.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.res.ColorStateList;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.RecipeIngredientAdapter;
import com.ifwaxtel.reciprada.adapter.RecipeStepAdapter;
import com.ifwaxtel.reciprada.adapter.RecipeToolAdapter;
import com.ifwaxtel.reciprada.adapter.SlidePagerAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentRecipeDetailBinding;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Ingredient;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.core.model.RecipeIngredient;
import com.ifwaxtel.reciprada.core.model.RecipeStep;
import com.ifwaxtel.reciprada.core.model.RecipeTool;
import com.ifwaxtel.reciprada.core.model.Tool;
import com.ifwaxtel.reciprada.viewmodel.RecipeDetailViewModel;
import com.ifwaxtel.reciprada.viewmodel.RecipeDetailViewModelRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * The type Recipe detail fragment.
 */
public class RecipeDetailFragment extends Fragment implements RecipeIngredientAdapter.RecyclerClickListener,
        RecipeToolAdapter.RecyclerClickListener, RecipeStepAdapter.RecyclerClickListener, View.OnClickListener{
    /**
     * The constant TAG.
     */
    public static final String TAG = RecipeDetailFragment.class.getSimpleName();

    private static final String RECIPE_OBJECT = "recipe_object";

    private MainListener.OnFragmentInteractionListener mListener;

    private Recipe recipe;

    private FragmentRecipeDetailBinding binding;
    private RecipeDetailViewModel viewModel;
    private RecipeIngredientAdapter recipeIngredientAdapter;
    private RecipeToolAdapter recipeToolAdapter;
    private RecipeStepAdapter recipeStepAdapter;

    /**
     * The M to right animation.
     */
    boolean mToRightAnimation;
    private boolean expandedView;
    private ViewGroup transitionsContainer;
    private ExecutorService executorService;


    private float proteinCount = 0.0f;
    private float calorieCount = 0.0f;
    private float carbCount = 0.0f;


    /**
     * Instantiates a new Recipe detail fragment.
     */
    public RecipeDetailFragment() {
        // Required empty public constructor
    }

    /**
     * New instance recipe detail fragment.
     *
     * @param _recipe the recipe
     * @return the recipe detail fragment
     */
    public static RecipeDetailFragment newInstance(Recipe _recipe) {
        RecipeDetailFragment fragment = new RecipeDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(RECIPE_OBJECT, _recipe);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recipe = getArguments().getParcelable(RECIPE_OBJECT);

            executorService = Executors.newSingleThreadExecutor();

            recipeIngredientAdapter = new RecipeIngredientAdapter(getContext(), this);
            recipeToolAdapter = new RecipeToolAdapter(getContext(), this);
            recipeStepAdapter = new RecipeStepAdapter(getActivity(), this);

            viewModel = ViewModelProviders.of(this, new RecipeDetailViewModelRepository(Objects.requireNonNull(getActivity()).getApplication(), recipe.getId(), recipe.isDetailsLoaded())).get(RecipeDetailViewModel.class);
            viewModel.getRecipeIngredients().observe(this, recipeIngredientsObserver);
            viewModel.getRecipeTools().observe(this, recipeToolsObserver);
            viewModel.getRecipeSteps().observe(this, recipeStepsObserver);
        }


    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recipe_detail, container, false);
        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        transitionsContainer = view.findViewById(R.id.details_fragment_container);
        setupViews();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainListener.OnFragmentInteractionListener) {
            mListener = (MainListener.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setupViews(){
        ((AppCompatActivity)Objects.requireNonNull(getActivity())).setSupportActionBar(binding.toolbar);

        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);


        SlidePagerAdapter adapter = new SlidePagerAdapter();
        adapter.setData(createPageList());

        binding.viewPager.setAdapter(adapter);

        binding.recipeDetailContent.recipeName.setText(recipe.getName());
        String duration = recipe.getDuration() + " mins";
        binding.recipeDetailContent.recipeCookingTime.setText(duration);
        binding.recipeDetailContent.recipeDifficulty.setText(recipe.getDifficulty());
        binding.recipeDetailContent.recipeDescription.setText(recipe.getDescription());

        binding.recipeDetailContent.recipeToolsRV.setNestedScrollingEnabled(false);
        binding.recipeDetailContent.recipeIngredientsRV.setNestedScrollingEnabled(false);

        if(recipe.isFavourite()){
            binding.favouriteFAB.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
        }else{
            binding.favouriteFAB.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.darker_gray)));
        }

        setupDetails();


        binding.favouriteFAB.setOnClickListener(this);
        binding.expandImageView.setOnClickListener(this);
    }

    private void setupDetails() {
        binding.recipeDetailContent.recipeToolsRV.setHasFixedSize(true);
        binding.recipeDetailContent.recipeToolsRV.setLayoutManager(new GridLayoutManager(getContext(), 4));
        binding.recipeDetailContent.recipeToolsRV.setAdapter(recipeToolAdapter);

        binding.recipeDetailContent.recipeIngredientsRV.setHasFixedSize(true);
        binding.recipeDetailContent.recipeIngredientsRV.setLayoutManager(new GridLayoutManager(getContext(), 4));
        binding.recipeDetailContent.recipeIngredientsRV.setAdapter(recipeIngredientAdapter);

        binding.recipeDetailContent.recipeStepsRV.setHasFixedSize(true);
        binding.recipeDetailContent.recipeStepsRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.recipeDetailContent.recipeStepsRV.setAdapter(recipeStepAdapter);
    }

    private Observer<List<RecipeIngredient>> recipeIngredientsObserver = new Observer<List<RecipeIngredient>>() {
        @Override
        public void onChanged(@Nullable List<RecipeIngredient> _recipeIngredients) {
            //recipeIngredientAdapter.addIngredients(recipeIngredients);

            if(_recipeIngredients != null && _recipeIngredients.size() > 0) {
                for (RecipeIngredient ingredient : _recipeIngredients) {

                    Callable<Ingredient> callable = () -> viewModel.getIngredientById(ingredient.getIngredientId());

                    Future<Ingredient> future = executorService.submit(callable);

                    try {
                        ingredient.setIngredient(future.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    if (ingredient.getIngredient() != null) {
                        proteinCount += (ingredient.getIngredient().getProteinCount() * ingredient.getQuantity());
                        calorieCount += (ingredient.getIngredient().getCalorieCount() * ingredient.getQuantity());
                        carbCount += (ingredient.getIngredient().getCarbohydrateCount() * ingredient.getQuantity());

                        Log.w("Calorie", "Observed " + (ingredient.getIngredient().getCalorieCount() * ingredient.getQuantity()));
                    }
                }
                recipeIngredientAdapter.addIngredients(_recipeIngredients);
                binding.recipeDetailContent.protein.setText(String.valueOf(proteinCount));
                binding.recipeDetailContent.calories.setText(String.valueOf(calorieCount));
                binding.recipeDetailContent.carbohydrate.setText(String.valueOf(calorieCount));

            }

            Log.w("Ingredients","Observed " + Objects.requireNonNull(_recipeIngredients).size());
        }
    };

    private Observer<List<RecipeTool>> recipeToolsObserver = new Observer<List<RecipeTool>>() {
        @Override
        public void onChanged(@Nullable List<RecipeTool> recipeTools) {

            if(recipeTools != null && recipeTools.size() > 0) {
                for (RecipeTool recipeTool : recipeTools) {
                    Callable<Tool> callable = () -> viewModel.getToolById(recipeTool.getToolId());

                    Future<Tool> future = executorService.submit(callable);

                    try {
                        recipeTool.setTool(future.get());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }

                recipeToolAdapter.addTools(recipeTools);
            }
            Log.w("Tool","Observed " + Objects.requireNonNull(recipeTools).size());
        }
    };

    private Observer<List<RecipeStep>> recipeStepsObserver = new Observer<List<RecipeStep>>() {
        @Override
        public void onChanged(@Nullable List<RecipeStep> recipeSteps) {
            recipeStepAdapter.addSteps(recipeSteps);
        }
    };

    @NonNull
    private List<View> createPageList() {
        List<View> pageList = new ArrayList<>();

        for(RecipeImage recipeImage : recipe.getRecipeImages()){
            pageList.add(createPageView(recipeImage.getImage()));
        }

        return pageList;
    }

    @NonNull
    private ImageView createPageView(String image) {
        ImageView imageView = new ImageView(getContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        GlideApp.with(Objects.requireNonNull(getActivity()))
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(withCrossFade())
                .into(imageView);

        return imageView;
    }

    @Override
    public void onItemClick(RecipeTool item, View view) {
        Objects.requireNonNull(getFragmentManager()).beginTransaction().replace(R.id.fragment_hook,
                ToolFragment.newInstance(item.getTool())).addToBackStack(ToolFragment.TAG).commit();
    }

    @Override
    public void onItemClick(RecipeIngredient item, View view) {
        Objects.requireNonNull(getFragmentManager()).beginTransaction().replace(R.id.fragment_hook,
                IngredientFragment.newInstance(item.getIngredient())).addToBackStack(ToolFragment.TAG).commit();
    }

    @Override
    public void onItemClick(RecipeStep item) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.favourite_FAB:
                if(!recipe.isFavourite()){
                    recipe.setFavourite(true);
                    binding.favouriteFAB.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.holo_red_dark)));
                }else{
                    recipe.setFavourite(false);
                    binding.favouriteFAB.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.darker_gray)));
                }
                viewModel.updateFavourite(recipe);
                break;
            case R.id.expand_image_view:
                TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                        .addTransition(new ChangeBounds()));

                expandedView = !expandedView;

                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 270,
                        getResources().getDisplayMetrics());
                ViewGroup.LayoutParams params = binding.appBarLayout.getLayoutParams();
                params.height = expandedView ? ViewGroup.LayoutParams.MATCH_PARENT : height;
                binding.appBarLayout.setLayoutParams(params);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Objects.requireNonNull(getFragmentManager()).popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
