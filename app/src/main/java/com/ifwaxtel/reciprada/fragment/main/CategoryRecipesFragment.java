package com.ifwaxtel.reciprada.fragment.main;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.RecipePagedAdapter;
import com.ifwaxtel.reciprada.databinding.FragmentCategoryRecipesBinding;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.utils.GridSpanSizeLookup;
import com.ifwaxtel.reciprada.viewmodel.CategoryRecipesViewModel;
import com.ifwaxtel.reciprada.viewmodel.CategoryRecipesViewModelRepository;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The type Category recipes fragment.
 */
public class CategoryRecipesFragment extends Fragment  {
    /**
     * The constant TAG.
     */
    public static final String TAG = CategoryRecipesFragment.class.getSimpleName();

    private MainListener.OnFragmentInteractionListener mListener;
    private RecipePagedAdapter.RecyclerClickListener recipeListener;
    private Category category;

    private FragmentCategoryRecipesBinding binding;
    private RecipePagedAdapter recipeAdapter;
    private CategoryRecipesViewModel recipesViewModel;
    private ExecutorService executorService;

    /**
     * Instantiates a new Category recipes fragment.
     */
    public CategoryRecipesFragment() {
        // Required empty public constructor
    }

    /**
     * New instance category recipes fragment.
     *
     * @param _category the category
     * @return the category recipes fragment
     */
    public static CategoryRecipesFragment newInstance(Category _category ) {

        CategoryRecipesFragment fragment = new CategoryRecipesFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("category", _category);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        category = Objects.requireNonNull(getArguments()).getParcelable("category");
        executorService = Executors.newSingleThreadExecutor();
        recipeAdapter = new RecipePagedAdapter(getActivity(), false, recipeListener);
        recipesViewModel = ViewModelProviders.of(this, new CategoryRecipesViewModelRepository(Objects.requireNonNull(getActivity()).getApplication(), category.getId())).get(CategoryRecipesViewModel.class);
        recipesViewModel.getRecipes().observe(this, recipes -> {
            if (recipes != null) {
                for (Recipe recipe : Objects.requireNonNull(recipes)) {
                    Callable<List<RecipeImage>> imageCallable = () -> recipesViewModel.loadRecipeImages(recipe.getId());
                    Callable<Category> categoryCallable = () -> recipesViewModel.loadRecipeCategory(recipe.getId());
                    try {
                        recipe.setRecipeImages(executorService.submit(imageCallable).get());
                        recipe.setCategory(executorService.submit(categoryCallable).get());
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    } catch (ExecutionException e) {
                        //e.printStackTrace();
                    }
                }
                recipeAdapter.submitList(recipes);
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_recipes
                , container, false);
        setHasOptionsMenu(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View v) {
        ((AppCompatActivity)Objects.requireNonNull(getActivity())).setSupportActionBar(binding.toolbar);
        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        binding.pageTitle.setText(category.getName());

        setOffset();

        setupRecipes();
    }

    private void setOffset() {
        binding.appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    binding.collapsingAppBar.setTitle(category.getName());
                    isShow = true;
                } else if (isShow) {
                    binding.collapsingAppBar.setTitle(" ");
                    isShow = false;
                }

            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainListener.OnFragmentInteractionListener) {
            mListener = (MainListener.OnFragmentInteractionListener) context;
            recipeListener = (RecipePagedAdapter.RecyclerClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        recipeListener = null;
    }

    private void setupRecipes() {
        binding.recipes.recyclerView.setNestedScrollingEnabled(true);
        binding.recipes.recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2, RecyclerView.VERTICAL, false);
        gridLayoutManager.setSpanSizeLookup(new GridSpanSizeLookup(5, 1, 2));
        binding.recipes.recyclerView.setLayoutManager(gridLayoutManager);
        //binding.homeContent.categoryRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        binding.recipes.recyclerView.setAdapter(recipeAdapter);

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Objects.requireNonNull(getFragmentManager()).popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
