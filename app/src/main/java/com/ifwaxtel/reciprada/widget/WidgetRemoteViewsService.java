package com.ifwaxtel.reciprada.widget;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * The type Widget remote views service.
 */
public class WidgetRemoteViewsService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new WidgetRemoteViewsRepository(this.getApplicationContext(), intent.getIntExtra("recipeId", 0));
    }
}
