package com.ifwaxtel.reciprada.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

/**
 * Implementation of App Widget functionality.
 */
public class RecipeIngredientsWidget extends AppWidgetProvider {


    /*void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                         int appWidgetId, Recipe _recipe, List<Ingredient> ingredients) {

        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.recipe_ingredients_widget);
        if (_recipe != null) {
            views.setTextViewText(R.id.recipe_name, _recipe.getName());

            if (ingredients.size() > 0){
                for (Ingredient ingredient : ingredients) {
                    String ing_measurement = ingredient.getQuantity() + " " + ingredient.getMeasure();

                    RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_item_ingredient);
                    remoteViews.setTextViewText(R.id.ingredient, ingredient.getIngredient());
                    remoteViews.setTextViewText(R.id.ingredient_measurement, ing_measurement);
                    views.addView(R.id.container, remoteViews);
                }
            }
        }


        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }*/

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them4
        /*RecipeRepository recipeRepository = new RecipeRepository(context);
        Recipe recipe = recipeRepository.fetchNextRecipe(0);




        /*List<Ingredient> ingredients = new ArrayList<>();
        if (recipe != null){
            ingredients = recipeRepository.fetchRecipeIngredientsList(recipe.getId());
        }

        for (int appWidgetId : appWidgetIds) {
            //updateAppWidget(context, appWidgetManager, appWidgetId, recipe, ingredients);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.recipe_ingredients_widget);
            if (recipe != null) {
                views.setTextViewText(R.id.recipe_name, recipe.getName());
            }


            Intent intent = new Intent(context, WidgetRemoteViewsService.class);
            intent.putExtra("recipeId", recipe.getId());

            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
            views.setRemoteAdapter(appWidgetId, R.id.widget_grid_view, intent);

            views.setEmptyView(R.id.widget_grid_view, R.id.empty_view);


            appWidgetManager.updateAppWidget(appWidgetId, views);
        }*/

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created

    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

