package com.ifwaxtel.reciprada.widget;

import android.content.Context;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.ifwaxtel.reciprada.core.model.Ingredient;

import java.util.List;

/**
 * The type Widget remote views repository.
 */
public class WidgetRemoteViewsRepository implements RemoteViewsService.RemoteViewsFactory {
    /**
     * The Context.
     */
    Context context;
    /**
     * The Recipe id.
     */
    int recipeId;
    /**
     * The Ingredients.
     */
    List<Ingredient> ingredients;

    /**
     * Instantiates a new Widget remote views repository.
     *
     * @param context   the context
     * @param _recipeId the recipe id
     */
    public WidgetRemoteViewsRepository(Context context, int _recipeId) {
        this.context = context;
        this.recipeId = _recipeId;
        //this.ingredients = new ArrayList<>();
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        //RecipeRepository recipeRepository = new RecipeRepository(context);
        //ingredients = recipeRepository.fetchRecipeIngredientsList(recipeId);
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public int getCount() {
        return ingredients.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        if (ingredients.size() == 0) {
            return null;
        }
        /*else{
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_item_ingredient);
            String ing_measurement = ingredients.get(i).getQuantity() + " " + ingredients.get(i).getMeasure();
            views.setTextViewText(R.id.ingredient, ingredients.get(i).getIngredient());
            views.setTextViewText(R.id.ingredient_measurement, ing_measurement);

            return views;
        }*/

        return null;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
