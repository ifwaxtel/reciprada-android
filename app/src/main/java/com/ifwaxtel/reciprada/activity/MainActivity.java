package com.ifwaxtel.reciprada.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.adapter.CategoryAdapter;
import com.ifwaxtel.reciprada.adapter.RecipeAdapter;
import com.ifwaxtel.reciprada.adapter.RecipePagedAdapter;
import com.ifwaxtel.reciprada.fragment.main.CategoriesFragment;
import com.ifwaxtel.reciprada.fragment.main.CategoryRecipesFragment;
import com.ifwaxtel.reciprada.fragment.main.FavouritesFragment;
import com.ifwaxtel.reciprada.fragment.main.HomeFragment;
import com.ifwaxtel.reciprada.fragment.main.RecipeDetailFragment;
import com.ifwaxtel.reciprada.fragment.main.RecipesFragment;
import com.ifwaxtel.reciprada.listeners.MainListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.utils.StoreUtil;

/**
 * The type Main activity.
 */
public class MainActivity extends AppCompatActivity implements MainListener.OnFragmentInteractionListener, CategoryAdapter.RecyclerClickListener, RecipePagedAdapter.RecyclerClickListener, RecipeAdapter.RecyclerClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                    HomeFragment.newInstance()).commit();
        }

    }

    /**
     * @param id
     */
    @Override
    public void onFragmentInteraction(int id) {
        switch (id){
            case R.id.nav_home:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        HomeFragment.newInstance()).commit();
                break;
            case R.id.nav_categories:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        CategoriesFragment.newInstance()).commit();
                break;
            case R.id.nav_recipes:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        RecipesFragment.newInstance()).commit();
                break;
            case R.id.nav_favourites:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        FavouritesFragment.newInstance()).commit();
                break;
            case R.id.nav_tips:
                break;
            case R.id.nav_settings:
                break;
            case R.id.nav_share:
                StoreUtil.ShareApp(getApplicationContext());
                break;
            case R.id.nav_rate:
                StoreUtil.RateApp(getApplicationContext());
                break;
        }
    }

    @Override
    public void onItemClick(Category item) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                CategoryRecipesFragment.newInstance(item)).addToBackStack(CategoryRecipesFragment.TAG).commit();
    }

    @Override
    public void onItemClick(Recipe item) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                RecipeDetailFragment.newInstance(item)).addToBackStack(RecipeDetailFragment.TAG).commit();
    }


}
