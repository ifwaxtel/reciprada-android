package com.ifwaxtel.reciprada.activity;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.service.BackgroundTaskService;
import com.ifwaxtel.reciprada.viewmodel.MainViewModel;

/**
 * The type Splash activity.
 */
public class SplashActivity extends AppCompatActivity implements RepositoryListener.TokenRetrieve{

    //boolean loggedIn = false;

    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //PrefUtil prefUtil = new PrefUtil(getApplicationContext());
        //loggedIn = prefUtil.getLoggedIn();

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.getToken(this);

    }

    @Override
    public void onFetchTokenSuccess(String token) {
        BackgroundTaskService.startActionFetchCategories(getApplicationContext(), "Bearer " + token);
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
        /*if(loggedIn){
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent = new Intent(SplashActivity.this, AuthActivity.class);
            startActivity(intent);
            finish();
        }*/
    }

    @Override
    public void onFetchFailed(String response) {
        final Snackbar snackBar = Snackbar.make(findViewById(R.id.splash), "Error: " + response, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Retry", v -> {
            snackBar.dismiss();
            mainViewModel.getToken(SplashActivity.this);
        });
        snackBar.show();
    }

}
