package com.ifwaxtel.reciprada.activity;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.databinding.ActivityAuthBinding;
import com.ifwaxtel.reciprada.fragment.auth.LandingFragment;
import com.ifwaxtel.reciprada.fragment.auth.LoginFragment;
import com.ifwaxtel.reciprada.fragment.auth.SignupMethodFragment;
import com.ifwaxtel.reciprada.fragment.auth.SignupWithEmailFragment;
import com.ifwaxtel.reciprada.listeners.AuthListener;
import com.ifwaxtel.reciprada.remote.response.UserResponse;
import com.ifwaxtel.reciprada.utils.DialogUtil;
import com.ifwaxtel.reciprada.utils.FacebookUtil;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.utils.GoogleUtil;
import com.ifwaxtel.reciprada.utils.PrefUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

import retrofit2.Response;

/**
 * The type Auth activity.
 */
public class AuthActivity extends AppCompatActivity implements AuthListener.OnFragmentInteractionListener, AuthListener.RemoteResponseInterface {


    private static final int G_SIGN_IN = 1001;
    private ActivityAuthBinding binding;
    private FacebookUtil facebookUtil;
    private GoogleUtil googleUtil;
    private DialogUtil dialogUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth);

        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                    LandingFragment.newInstance()).commit();
        }
        dialogUtil = new DialogUtil();

        facebookUtil = new FacebookUtil();
        facebookUtil.facebookLoginSetup(this);

        googleUtil = new GoogleUtil(AuthActivity.this);
        googleUtil.googleLoginSetup();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        dialogUtil.getFixedDialog(this,"Logging in...").show();
        facebookUtil.getCallbackManager().onActivityResult(requestCode, resultCode, data);
        if (requestCode == G_SIGN_IN) {
            googleUtil.handleGoogleResponse(data,this);
        }
    }

    /*Handle fragment interactions
     * By setting the ids of each form to be the same we can just pass views and have
     * this function handle them from the main activity
     * The actions here can also be handled from the individual fragments
     */
    @Override
    public void onFragmentInteraction(View v) {
        switch (v.getId()){
            case R.id.skip_button:
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.login:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        LoginFragment.newInstance(this, dialogUtil)).addToBackStack(LoginFragment.TAG).commit();
                break;
            case R.id.signup:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        SignupMethodFragment.newInstance()).addToBackStack(SignupMethodFragment.TAG).commit();
                break;
            case R.id.signup_with_email:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_hook,
                        SignupWithEmailFragment.newInstance(this, dialogUtil)).addToBackStack(SignupWithEmailFragment.TAG).commit();
                break;
            case R.id.signup_with_facebook:
                LoginManager.getInstance().logInWithReadPermissions(AuthActivity.this, Arrays.asList("public_profile",GlobalConstants.EMAIL));
                break;
            case R.id.signup_with_google:
                startActivityForResult(googleUtil.getGoogleSignInClient().getSignInIntent(), G_SIGN_IN);
                break;
            case R.id.back_button:
                getSupportFragmentManager().popBackStackImmediate();
                break;

        }
    }

    @Override
    public void onResponse(Response<UserResponse> response) {
        dialogUtil.closeDialog();
        if (response.isSuccessful()){

            //persist the returned user data
            //preference was used instead of sqlite because it has less overhead
            PrefUtil prefUtil = new PrefUtil(getApplicationContext());
            prefUtil.saveUserData(response.body(),getApplicationContext());

            startActivity(new Intent(getApplicationContext(), MainActivity.class));

        }
        else if(response.code() == 401){
            //Parse the responseBody when an error code 401 is retured from the server
            try {
                DialogUtil.displayInfoOnSnackBar(binding.fragmentHook, new Gson().fromJson(
                        new JsonParser().parse(Objects.requireNonNull(response.errorBody()).string()),
                        UserResponse.class).getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else{
            DialogUtil.displayInfoOnSnackBar(binding.fragmentHook,getString(R.string.process_request_failed));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        dialogUtil.closeDialog();
        DialogUtil.displayInfoOnSnackBar(binding.fragmentHook,t.getMessage());
    }
}
