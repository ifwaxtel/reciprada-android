package com.ifwaxtel.reciprada.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.remote.response.UserResponse;

/**
 * The type Pref util.
 */
public class PrefUtil {
    private SharedPreferences settings;

    /**
     * Instantiates a new Pref util.
     *
     * @param context the context
     */
    public PrefUtil(Context context) {
        this.settings = PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Save user data.
     *
     * @param user    the user
     * @param context the context
     */
    public void saveUserData(UserResponse user, Context context){

        SharedPreferences.Editor editor = this.getEditor();
        this.setLoggedIn(editor);
        this.setId(editor,user.getId());
        this.setFirstName(editor,user.getFirstName());
        this.setLastName(editor,user.getLastName());
        this.setEmail(editor,user.getEmail());
        this.setProfilePicture(editor,user.getProfilePicture());
        this.setCountry(editor,user.getCountry());
        this.setState(editor,user.getState());
        this.setCountryCode(editor,user.getCountryCode());
        this.setPhoneNumber(editor,user.getPhoneNumber());
        this.setDob(editor,user.getDob());
        this.setCreatedAt(editor,user.getCreatedAt());
        this.setUpdatedAt(editor,user.getUpdatedAt());
        this.setToken(editor,user.getToken());
        this.setGender(editor,user.getGender());

        this.commitEdit(editor);
    }

    /**
     * Get editor shared preferences . editor.
     *
     * @return the shared preferences . editor
     */
    public SharedPreferences.Editor getEditor(){
        return settings.edit();
    }

    /**
     * Commit edit.
     *
     * @param editor the editor
     */
    public void commitEdit(SharedPreferences.Editor editor){
        editor.commit();
    }

    private void setLoggedIn(SharedPreferences.Editor editor){
        editor.putBoolean(GlobalConstants.LOGGED_IN,true);
    }

    /**
     * Sets request token.
     *
     * @param editor the editor
     * @param token  the token
     */
    public void setRequestToken(SharedPreferences.Editor editor, String token) {
        editor.putString(GlobalConstants.REQUEST_TOKEN, token);
    }

    /**
     * Gets request token.
     *
     * @return the request token
     */
    public String getRequestToken() {
        return settings.getString(GlobalConstants.REQUEST_TOKEN, null);
    }

    /**
     * Get logged in boolean.
     *
     * @return the boolean
     */
    public boolean getLoggedIn(){
        return settings.getBoolean(GlobalConstants.LOGGED_IN,false);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return settings.getInt(GlobalConstants.ID,0);
    }

    private void setId(SharedPreferences.Editor editor, int id) {
        editor.putInt(GlobalConstants.ID, id);
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return settings.getString(GlobalConstants.FIRST_NAME, null);
    }

    private void setFirstName(SharedPreferences.Editor editor, String firstName) {
        editor.putString(GlobalConstants.ID, firstName);
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return settings.getString(GlobalConstants.LAST_NAME, null);
    }

    private void setLastName(SharedPreferences.Editor editor, String lastName) {
        editor.putString(GlobalConstants.ID, lastName);
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return settings.getString(GlobalConstants.EMAIL, null);
    }

    private void setEmail(SharedPreferences.Editor editor, String email) {
        editor.putString(GlobalConstants.EMAIL, email);
    }

    /**
     * Gets gender.
     *
     * @return the gender
     */
    public String getGender() {
        return settings.getString(GlobalConstants.GENDER, null);
    }

    private void setGender(SharedPreferences.Editor editor, String gender) {
        editor.putString(GlobalConstants.ID, gender);
    }

    /**
     * Gets dob.
     *
     * @return the dob
     */
    public String getDob() {
        return settings.getString(GlobalConstants.DOB, null);
    }

    private void setDob(SharedPreferences.Editor editor, String dob) {
        editor.putString(GlobalConstants.DOB, dob);
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return settings.getString(GlobalConstants.COUNTRY, null);
    }

    private void setCountry(SharedPreferences.Editor editor, String country) {
        editor.putString(GlobalConstants.COUNTRY, country);
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return settings.getString(GlobalConstants.STATE, null);
    }

    private void setState(SharedPreferences.Editor editor, String state) {
        editor.putString(GlobalConstants.STATE, state);
    }

    /**
     * Gets country code.
     *
     * @return the country code
     */
    public String getCountryCode() {
        return settings.getString(GlobalConstants.COUNTRY_CODE, null);
    }

    private void setCountryCode(SharedPreferences.Editor editor, String countryCode) {
        editor.putString(GlobalConstants.COUNTRY_CODE, countryCode);
    }

    /**
     * Gets phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return settings.getString(GlobalConstants.PHONE_NUMBER, null);
    }

    private void setPhoneNumber(SharedPreferences.Editor editor, String phoneNumber) {
        editor.putString(GlobalConstants.PHONE_NUMBER, phoneNumber);
    }

    /**
     * Gets profile picture.
     *
     * @return the profile picture
     */
    public String getProfilePicture() {
        return settings.getString(GlobalConstants.PROFILE_PICTURE, null);
    }

    private void setProfilePicture(SharedPreferences.Editor editor, String profilePicture) {
        editor.putString(GlobalConstants.PROFILE_PICTURE, profilePicture);
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public String getCreatedAt() {
        return settings.getString(GlobalConstants.CREATED_AT, null);
    }

    private void setCreatedAt(SharedPreferences.Editor editor, String createdAt) {
        editor.putString(GlobalConstants.CREATED_AT, createdAt);
    }

    /**
     * Gets updated at.
     *
     * @return the updated at
     */
    public String getUpdatedAt() {
        return settings.getString(GlobalConstants.UPDATED_AT, null);
    }

    private void setUpdatedAt(SharedPreferences.Editor editor, String updatedAt) {
        editor.putString(GlobalConstants.UPDATED_AT, updatedAt);
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return settings.getString(GlobalConstants.TOKEN, null);
    }

    /**
     * Sets token.
     *
     * @param editor the editor
     * @param token  the token
     */
    public void setToken(SharedPreferences.Editor editor, String token) {
        editor.putString(GlobalConstants.TOKEN, token);
    }
}
