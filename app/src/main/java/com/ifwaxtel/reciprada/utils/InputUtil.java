package com.ifwaxtel.reciprada.utils;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

/**
 * The type Input util.
 */
public class InputUtil {

    /**
     * Is valid email boolean.
     *
     * @param target the target
     * @return the boolean
     */
    public static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Is valid mobile boolean.
     *
     * @param phone the phone
     * @return the boolean
     */
    public static boolean isValidMobile(String phone) {
        return !TextUtils.isEmpty(phone) && android.util.Patterns.PHONE.matcher(phone).matches() && phone.length() > 8;
    }

    /**
     * Is valid length boolean.
     *
     * @param data   the data
     * @param length the length
     * @return the boolean
     */
    public static boolean isValidLength(String data, int length){
        return data.length() >= length;
    }

    /**
     * Reset error.
     *
     * @param textInput  the text input
     * @param textLayout the text layout
     */
    public static void resetError(TextInputEditText textInput, final TextInputLayout textLayout){
        textInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLayout.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
