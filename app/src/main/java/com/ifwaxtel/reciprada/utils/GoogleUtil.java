package com.ifwaxtel.reciprada.utils;

import android.app.Activity;
import android.content.Intent;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.utils.RandomUtil;
import com.ifwaxtel.reciprada.listeners.AuthListener;

import java.util.Objects;

/**
 * The type Google util.
 */
public class GoogleUtil {
    private GoogleSignInClient mGoogleSignInClient;
    private Activity context;

    /**
     * Instantiates a new Google util.
     *
     * @param _context the context
     */
    public GoogleUtil(Activity _context) {
        this.context = _context;
    }

    /**
     * Google login setup.
     */
    public void googleLoginSetup(){
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(GlobalConstants.GOOGLE_API_KEY)
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(context, googleSignInOptions);
    }

    /**
     * Handle google response.
     *
     * @param data           the data
     * @param remoteListener the remote listener
     */
    public void handleGoogleResponse(Intent data,final AuthListener.RemoteResponseInterface remoteListener){
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);

            AuthUtil authUtil = new AuthUtil(remoteListener);
            authUtil.registerSocial(Objects.requireNonNull(account).getGivenName(),account.getFamilyName(),null,
                    null,account.getEmail(),RandomUtil.randomPassword(7));

        } catch (ApiException e) {
            remoteListener.onFailure(e);
        }
    }

    /**
     * Gets google sign in client.
     *
     * @return the google sign in client
     */
    public GoogleSignInClient getGoogleSignInClient() {
        return mGoogleSignInClient;
    }
}
