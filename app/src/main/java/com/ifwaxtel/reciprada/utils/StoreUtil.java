package com.ifwaxtel.reciprada.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

/***
 * A set of methods that will allow users share and rate the app on googles play store
 */
public class StoreUtil {

    /**
     * Share app.
     *
     * @param context the context
     */
    public static void ShareApp(Context context){
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Reciprada");
            String shareText = "\nCheck this awesome recipes application out\n\n";
            shareText = shareText + "https://play.google.com/store/apps/details?com.ifwaxtel.reciprada \n\n";
            i.putExtra(Intent.EXTRA_TEXT, shareText);
            context.startActivity(Intent.createChooser(i, "Share Reciprada via.."));
        } catch(Exception e) {
            //e.toString();
        }
    }

    /**
     * Rate app.
     *
     * @param context the context
     */
    public static void RateApp(Context context)
    {
        try
        {
            Intent rateIntent = RateIntentForUri("market://details", context);
            context.startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e)
        {
            Intent rateIntent = RateIntentForUri("https://play.google.com/store/apps/details", context);
            context.startActivity(rateIntent);
        }
    }

    private static Intent RateIntentForUri(String url, Context context)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, context.getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
        {
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
        }
        else
        {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }
}
