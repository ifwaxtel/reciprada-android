package com.ifwaxtel.reciprada.utils;

import android.os.Bundle;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.utils.RandomUtil;
import com.ifwaxtel.reciprada.listeners.AuthListener;

import org.json.JSONException;

/**
 * The type Facebook util.
 */
public class FacebookUtil {


    private CallbackManager mCallbackManager;


    /**
     * Instantiates a new Facebook util.
     */
    public FacebookUtil() {
        mCallbackManager = CallbackManager.Factory.create();
    }

    /**
     * Facebook login setup.
     *
     * @param remoteListener the remote listener
     */
    public void facebookLoginSetup(final AuthListener.RemoteResponseInterface remoteListener){

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(), (object, response) -> {
                                    try {
                                        String email = response.getJSONObject().getString(GlobalConstants.EMAIL);
                                        String firstName = response.getJSONObject().getString(GlobalConstants.FIRST_NAME);
                                        String lastName = response.getJSONObject().getString(GlobalConstants.LAST_NAME);

                                        AuthUtil authUtil = new AuthUtil(remoteListener);
                                        authUtil.registerSocial(firstName, lastName, null,
                                                null,email,RandomUtil.randomPassword(7));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        remoteListener.onFailure(e);
                                    }
                                }
                        );

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", GlobalConstants.EMAIL + "," +
                                GlobalConstants.FIRST_NAME + "," + GlobalConstants.LAST_NAME);
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        remoteListener.onFailure(new Exception("Login cancelled"));
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        remoteListener.onFailure(exception);
                    }
                }
        );
    }

    /**
     * Gets callback manager.
     *
     * @return the callback manager
     */
    public CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

}
