package com.ifwaxtel.reciprada.utils;

import android.content.Context;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.util.AttributeSet;

/**
 * The type Custom floating action button.
 */
public class CustomFloatingActionButton extends FloatingActionButton {
    /**
     * Instantiates a new Custom floating action button.
     *
     * @param context      the context
     * @param attrs        the attrs
     * @param defStyleAttr the def style attr
     */
    public CustomFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
