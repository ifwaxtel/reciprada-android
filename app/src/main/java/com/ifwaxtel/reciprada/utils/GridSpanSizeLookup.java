package com.ifwaxtel.reciprada.utils;

import androidx.recyclerview.widget.GridLayoutManager;

/**
 * The type Grid span size lookup.
 */
public class GridSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    private int spanPos;
    private int spanCnt1;
    private int spanCnt2;

    /**
     * Instantiates a new Grid span size lookup.
     *
     * @param spanPos  the span pos
     * @param spanCnt1 the span cnt 1
     * @param spanCnt2 the span cnt 2
     */
    public GridSpanSizeLookup(int spanPos, int spanCnt1, int spanCnt2) {
        super();
        this.spanPos = spanPos;
        this.spanCnt1 = spanCnt1;
        this.spanCnt2 = spanCnt2;
    }

    @Override
    public int getSpanSize(int position) {
        //return (position % spanPos == 0 ? spanCnt2 : spanCnt1);
        if (position % 3 == 2) {
            return 2;
        }
        switch (position % 4) {
            case 1:
            case 3:
                return 1;
            case 0:
            case 2:
                return 1;
            default:
                //never gonna happen
                return  -1 ;
        }
    }
}
