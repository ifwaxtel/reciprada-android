package com.ifwaxtel.reciprada.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

/***
 * This class handles all methods that are used to get device related data
 */
class DeviceUtil {

    /**
     * Get device id string.
     *
     * @param context the context
     * @return the string
     */
    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
