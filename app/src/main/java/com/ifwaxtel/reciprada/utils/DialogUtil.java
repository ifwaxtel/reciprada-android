package com.ifwaxtel.reciprada.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;

/**
 * The type Dialog util.
 */
public class DialogUtil {

    private ProgressDialog pd;

    /**
     * Get fixed dialog progress dialog.
     *
     * @param context the context
     * @param message the message
     * @return the progress dialog
     */
    public ProgressDialog getFixedDialog(Activity context, String message){
        pd = new ProgressDialog(context);
        pd.setMessage(message);
        pd.setCancelable(false);

        return pd;
    }

    /**
     * Close dialog.
     */
    public void closeDialog(){
        if(pd != null){
            pd.dismiss();
        }
    }

    /**
     * Display info on snack bar.
     *
     * @param view the view
     * @param info the info
     */
    public static void displayInfoOnSnackBar(View view, String info){
        Snackbar.make(view, info, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Display permanent info on snack bar.
     *
     * @param view the view
     * @param info the info
     */
    public static void displayPermanentInfoOnSnackBar(View view, String info){
        Snackbar.make(view, info, Snackbar.LENGTH_INDEFINITE).show();
    }
}
