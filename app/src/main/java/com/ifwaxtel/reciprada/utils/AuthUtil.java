package com.ifwaxtel.reciprada.utils;

import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.listeners.AuthListener;
import com.ifwaxtel.reciprada.remote.request.ApiClient;
import com.ifwaxtel.reciprada.remote.request.ApiService;
import com.ifwaxtel.reciprada.remote.response.UserResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The type Auth util.
 */
public class AuthUtil {
    private AuthListener.RemoteResponseInterface remoteListener;

    /**
     * Instantiates a new Auth util.
     *
     * @param _remoteListener the remote listener
     */
    public AuthUtil(AuthListener.RemoteResponseInterface _remoteListener) {
        this.remoteListener = _remoteListener;
    }

    /**
     * Login.
     *
     * @param email    the email
     * @param password the password
     */
    public void login(String email, String password){

        ApiClient.getClient().create(ApiService.class).login(email,password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                remoteListener.onResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                remoteListener.onFailure(t);
            }
        });

    }

    /**
     * Register.
     *
     * @param firstName   the first name
     * @param lastName    the last name
     * @param countryCode the country code
     * @param phone       the phone
     * @param email       the email
     * @param password    the password
     */
    public void register(String firstName, String lastName, String countryCode, String phone,
                         String email, String password){

        ApiClient.getClient().create(ApiService.class).register(firstName,lastName,countryCode,phone,email,password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                remoteListener.onResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                remoteListener.onFailure(t);
            }
        });

    }

    /**
     * Register social.
     *
     * @param firstName   the first name
     * @param lastName    the last name
     * @param countryCode the country code
     * @param phone       the phone
     * @param email       the email
     * @param password    the password
     */
    public void registerSocial(String firstName, String lastName, String countryCode, String phone,
                         String email, String password){

        ApiClient.getClient().create(ApiService.class).registerSocial(firstName,lastName,countryCode,phone,email,password).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                remoteListener.onResponse(response);
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                remoteListener.onFailure(t);
            }
        });

    }


}
