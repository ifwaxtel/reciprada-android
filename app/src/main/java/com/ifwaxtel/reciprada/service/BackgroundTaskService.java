package com.ifwaxtel.reciprada.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.database.RecipradaDatabase;
import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.repository.RemoteRepository;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class BackgroundTaskService extends IntentService implements RepositoryListener.CategoriesRetrieve, RepositoryListener.RecipesRetrieve {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FETCH_CATEGORIES = "com.ifwaxtel.reciprada.service.action.CATEGORIES";
    private static final String ACTION_FETCH_RECIPES = "com.ifwaxtel.reciprada.service.action.RECIPES";
    private static final String ACTION_FETCH_RECIPES_BY_CATEGORY = "com.ifwaxtel.reciprada.service.action.RECIPES_BY_CATEGORY";
    private static final String ACTION_FETCH_MORE_RECIPES = "com.ifwaxtel.reciprada.service.action.MORE_RECIPES";
    private static final String ACTION_FETCH_MORE_RECIPES_BY_CATEGORY = "com.ifwaxtel.reciprada.service.action.MORE_RECIPES_BY_CATEGORY";
    private static final String TOKEN = "token";
    private static final String CATEGORY_ID = "category_id";

    /**
     * Instantiates a new Background task service.
     */
    public BackgroundTaskService() {
        super("BackgroundTaskService");
    }

    /**
     * Starts this service to perform action ACTION_FETCH_CATEGORIES with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context       the context
     * @param request_token the request token
     * @see IntentService
     */
    public static void startActionFetchCategories(Context context, String request_token){
        Intent intent = new Intent(context, BackgroundTaskService.class);
        intent.setAction(ACTION_FETCH_CATEGORIES);
        intent.putExtra(TOKEN, request_token);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action ACTION_FETCH_CATEGORIES with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context       the context
     * @param request_token the request token
     * @see IntentService
     */
    public static void startActionFetchRecipes(Context context, String request_token){
        Intent intent = new Intent(context, BackgroundTaskService.class);
        intent.setAction(ACTION_FETCH_RECIPES);
        intent.putExtra(TOKEN, request_token);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action ACTION_FETCH_CATEGORIES with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context       the context
     * @param request_token the request token
     * @param category_id   the category id
     * @see IntentService
     */
    public static void startActionFetchRecipesByCategory(Context context, String request_token, int category_id){
        Intent intent = new Intent(context, BackgroundTaskService.class);
        intent.setAction(ACTION_FETCH_RECIPES_BY_CATEGORY);
        intent.putExtra(TOKEN, request_token);
        intent.putExtra(CATEGORY_ID, category_id);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action ACTION_FETCH_CATEGORIES with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context       the context
     * @param request_token the request token
     * @see IntentService
     */
    public static void startActionFetchMoreRecipes(Context context, String request_token){
        Intent intent = new Intent(context, BackgroundTaskService.class);
        intent.setAction(ACTION_FETCH_MORE_RECIPES);
        intent.putExtra(TOKEN, request_token);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action ACTION_FETCH_CATEGORIES with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @param context       the context
     * @param request_token the request token
     * @param category_id   the category id
     * @see IntentService
     */
    public static void startActionFetchMoreRecipesByCategory(Context context, String request_token, int category_id){
        Intent intent = new Intent(context, BackgroundTaskService.class);
        intent.setAction(ACTION_FETCH_MORE_RECIPES_BY_CATEGORY);
        intent.putExtra(TOKEN, request_token);
        intent.putExtra(CATEGORY_ID, category_id);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_CATEGORIES.equals(action)) {
                handleActionFetchCategories(intent.getStringExtra(TOKEN));
            }
            else if (ACTION_FETCH_RECIPES.equals(action)){
                handleActionFetchRecipes(intent.getStringExtra(TOKEN));
            }
            else if (ACTION_FETCH_RECIPES_BY_CATEGORY.equals(action)){
                handleActionFetchRecipesByCategory(intent.getStringExtra(TOKEN), intent.getIntExtra(CATEGORY_ID, 0));
            }
            else if (ACTION_FETCH_MORE_RECIPES.equals(action)){
                handleActionFetchMoreRecipes(intent.getStringExtra(TOKEN));
            }
            else if (ACTION_FETCH_MORE_RECIPES_BY_CATEGORY.equals(action)){
                handleActionFetchMoreRecipesByCategory(intent.getStringExtra(TOKEN), intent.getIntExtra(CATEGORY_ID, 0));
            }
        }
    }

    /**
     * Handle action ACTION_FETCH_CATEGORIES in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchCategories(String request_token) {
        RemoteRepository repository = new RemoteRepository(getApplication());
        repository.loadCategoriesFromApi(request_token, this, 0);
    }

    /**
     * Handle action ACTION_FETCH_CATEGORIES in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchRecipes(String request_token) {
        new RemoteRepository(getApplication()).loadRecipesFromApi(request_token, this, 0);
    }

    /**
     * Handle action ACTION_FETCH_CATEGORIES in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchRecipesByCategory(String request_token, int category_id) {
        new RemoteRepository(getApplication()).loadRecipesByCategoryFromApi(request_token, this, 0, category_id);
    }

    /**
     * Handle action ACTION_FETCH_CATEGORIES in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchMoreRecipes(String request_token) {
        int page = Math.round(this.recipeCount() / GlobalConstants.API_ITEMS_LIMIT) + 1;
        new RemoteRepository(getApplication()).loadRecipesFromApi(request_token, this, page);
    }

    /**
     * Handle action ACTION_FETCH_CATEGORIES in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchMoreRecipesByCategory(String request_token, int category_id) {
        int page = Math.round(this.recipeByCategoryCount(category_id) / GlobalConstants.API_ITEMS_LIMIT) + 1;
        new RemoteRepository(getApplication()).loadRecipesByCategoryFromApi(request_token, this, page, category_id);
    }

    @Override
    public void onFetchCategoriesSuccess() {
        Log.w("BackgroundTaskService", "Categories fetched");
    }

    @Override
    public void onFetchRecipesSuccess() {

    }

    @Override
    public void onFetchFailed(String response) {
        Log.w("BackgroundTaskService", response);
    }

    private int recipeByCategoryCount(int categoryId){
        RecipradaDatabase database = RecipradaDatabase.getDatabase(getApplication());
        return database.recipeDao().getRecipesByCategoryCount(categoryId);
    }

    private int recipeCount(){
        RecipradaDatabase database = RecipradaDatabase.getDatabase(getApplication());
        return database.recipeDao().getRecipesCount();
    }
}
