package com.ifwaxtel.reciprada.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.core.model.Recipe;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Orji Ifeanyi C. on 30/06/2018.
 */
public class RecipeAdapter extends RecyclerView.Adapter {
    private LayoutInflater mInflater;
    private List<Recipe> mResults;
    private RecyclerClickListener recyclerClickListener;
    private Activity context;
    private boolean twoSide;

    /**
     * Instantiates a new Recipe adapter.
     *
     * @param context  the context
     * @param listener the listener
     */
    public RecipeAdapter(Activity context, RecyclerClickListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResults = new ArrayList<>();
        this.context = context;
        this.recyclerClickListener = listener;
    }

    /**
     * Add recipes.
     *
     * @param _recipes the recipes
     */
    public void addRecipes(List<Recipe> _recipes) {
        //mResults.clear();
        //mResultsFiltered.clear();
        if (mResults.size() != 0) {
            for (Recipe recipe : _recipes) {
                if (mResults.contains(recipe)) {
                    //mResults.indexOf(recipe);
                    mResults.set(mResults.indexOf(recipe), recipe);
                    Log.w("Recipes", "replaced " + recipe.getName());
                } else {
                    mResults.add(recipe);
                    Log.w("Recipes", "added " + recipe.getName());
                }
            }
        } else {
            mResults.addAll(_recipes);
        }

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecipeAdapter.ItemViewHolder(mInflater.inflate(R.layout.item_recipe, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            final Recipe recipe = mResults.get(position);
            ((ItemViewHolder) holder).recipeName.setText(recipe.getName());
            ((ItemViewHolder) holder).recipeDescription.setText(recipe.getDescription());
            String duration = recipe.getDuration() + " mins";
            ((ItemViewHolder) holder).recipeTime.setText(duration);
            ((ItemViewHolder) holder).recipeDifficulty.setText(recipe.getDifficulty());

            if (recipe.getCategory() != null) {
                ((ItemViewHolder) holder).recipeCategory.setText(recipe.getCategory().getName());
            }

            if (recipe.getRecipeImages() != null) {
                if (recipe.getRecipeImages().size() > 0) {

                    GlideApp.with(context)
                            .load(recipe.getRecipeImages().get(0).getImage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transition(withCrossFade())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    ((ItemViewHolder) holder).loadingImage.setVisibility(View.GONE);
                                    ((ItemViewHolder) holder).errorImage.setVisibility(View.VISIBLE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    ((ItemViewHolder) holder).loadingImage.setVisibility(View.GONE);
                                    ((ItemViewHolder) holder).errorImage.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(((ItemViewHolder) holder).recipeImage);


                } else {
                    ((ItemViewHolder) holder).loadingImage.setVisibility(View.GONE);
                    ((ItemViewHolder) holder).errorImage.setVisibility(View.VISIBLE);
                    ((ItemViewHolder) holder).recipeImage.setImageDrawable(null);
                }
            }
        }

    }

    private Recipe getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Recipe image.
         */
        ImageView recipeImage, /**
         * The Error image.
         */
        errorImage;
        /**
         * The Recipe rating.
         */
        RatingBar recipeRating;
        /**
         * The Loading image.
         */
        ProgressBar loadingImage;
        /**
         * The Recipe name.
         */
        TextView recipeName, /**
         * The Recipe description.
         */
        recipeDescription, /**
         * The Recipe category.
         */
        recipeCategory, /**
         * The Recipe time.
         */
        recipeTime, /**
         * The Recipe difficulty.
         */
        recipeDifficulty;

        /**
         * Instantiates a new Item view holder.
         *
         * @param view the view
         */
        ItemViewHolder(View view) {
            super(view);

            doViewAdjustSize(view);

            recipeName = view.findViewById(R.id.recipe_name);
            recipeDescription = view.findViewById(R.id.recipe_description);
            recipeCategory = view.findViewById(R.id.recipe_category);
            recipeTime = view.findViewById(R.id.recipe_time);
            recipeDifficulty = view.findViewById(R.id.recipe_difficulty);
            recipeImage = view.findViewById(R.id.recipe_image);
            recipeRating = view.findViewById(R.id.recipe_rating);
            errorImage = view.findViewById(R.id.error_image);
            loadingImage = view.findViewById(R.id.image_loading);

            view.setOnClickListener(v -> recyclerClickListener.onItemClick(getItem(getAdapterPosition())));
        }
    }

    private void doViewAdjustSize(View view) {
        //Set the with of the list item to 70% of the phones screen
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        double widthPercentage = width * 0.70;

        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = ((int) widthPercentage);
        view.setLayoutParams(params);
    }

    /**
     * The interface Recycler click listener.
     */
    public interface RecyclerClickListener {
        /**
         * On item click.
         *
         * @param item the item
         */
        void onItemClick(Recipe item);
    }
}