package com.ifwaxtel.reciprada.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.core.model.RecipeStep;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Orji Ifeanyi C. on 30/06/2018.
 */
public class RecipeStepAdapter extends RecyclerView.Adapter<RecipeStepAdapter.ViewHolder>{
    private LayoutInflater mInflater;
    private List<RecipeStep> mResults;
    private RecyclerClickListener recyclerClickListener;
    private Activity context;

    /**
     * Instantiates a new Recipe step adapter.
     *
     * @param context  the context
     * @param listener the listener
     */
    public RecipeStepAdapter(Activity context, RecyclerClickListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResults = new ArrayList<>();
        this.context = context;
        this.recyclerClickListener = listener;
    }

    /**
     * Add steps.
     *
     * @param _recipeSteps the recipe steps
     */
    public void addSteps(List<RecipeStep> _recipeSteps){
        mResults.clear();
        mResults.addAll(_recipeSteps);
        this.notifyDataSetChanged();
    }

    /**
     * Instantiates a new Recipe step adapter.
     */
    public RecipeStepAdapter(){

    }

    @NonNull
    @Override
    public RecipeStepAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_recipe_step, parent, false);
        RecipeStepAdapter.ViewHolder holder = new RecipeStepAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeStepAdapter.ViewHolder holder, int position) {
        final RecipeStep recipeStep = mResults.get(position);
        holder.stepDescription.setText(recipeStep.getDescription());
        holder.stepNumber.setText(String.valueOf(position + 1));

        GlideApp.with(context)
                .load(recipeStep.getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transition(withCrossFade())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        holder.loadingImage.setVisibility(View.GONE);
                        holder.errorImage.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.loadingImage.setVisibility(View.GONE);
                        holder.errorImage.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(holder.stepImage);

    }

    private RecipeStep getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }


    /**
     * The type View holder.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Step description.
         */
        TextView stepDescription, /**
         * The Step number.
         */
        stepNumber;
        /**
         * The Step image.
         */
        ImageView stepImage, /**
         * The Error image.
         */
        errorImage;
        /**
         * The Loading image.
         */
        ProgressBar loadingImage;

        /**
         * Instantiates a new View holder.
         *
         * @param view the view
         */
        ViewHolder(View view) {
            super(view);

            doViewAdjustSize(view);

            stepDescription = view.findViewById(R.id.step_description);
            stepImage = view.findViewById(R.id.step_image);
            stepNumber = view.findViewById(R.id.stepNumber);

            errorImage = view.findViewById(R.id.error_image);
            loadingImage = view.findViewById(R.id.image_loading);

            view.setOnClickListener(v -> {
                final int position = getAdapterPosition();
                recyclerClickListener.onItemClick(getItem(position));
            });
        }
    }

    private void doViewAdjustSize(View view){
        //Set the with of the list item to 70% of the phones screen
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        double widthPercentage = width * 0.80;

        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = ((int) widthPercentage);
        view.setLayoutParams(params);
    }

    /**
     * The interface Recycler click listener.
     */
    public interface RecyclerClickListener{
        /**
         * On item click.
         *
         * @param item the item
         */
        void onItemClick(RecipeStep item);
    }
}
