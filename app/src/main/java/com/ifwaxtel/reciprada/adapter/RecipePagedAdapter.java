package com.ifwaxtel.reciprada.adapter;

import android.app.Activity;
import androidx.paging.PagedListAdapter;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.core.model.Recipe;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * The type Recipe paged adapter.
 */
public class RecipePagedAdapter extends PagedListAdapter<Recipe, RecipePagedAdapter.RecipeViewHolder> {

    private Activity activity;
    private boolean twoSide;
    private RecipePagedAdapter.RecyclerClickListener listener;
    /**
     * DiffUtil to compare the Recipe data (old and new)
     * for issuing notify commands suitably to update the list
     */
    private static DiffUtil.ItemCallback<Recipe> RECIPE_COMPARATOR
            = new DiffUtil.ItemCallback<Recipe>() {
        @Override
        public boolean areItemsTheSame(Recipe oldItem, Recipe newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(Recipe oldItem, @NonNull Recipe newItem) {
            return oldItem.equals(newItem);
        }
    };

    /**
     * Instantiates a new Recipe paged adapter.
     *
     * @param activity the activity
     * @param twoSide  the two side
     * @param listener the listener
     */
    public RecipePagedAdapter(Activity activity, boolean twoSide, RecipePagedAdapter.RecyclerClickListener listener) {
        super(RECIPE_COMPARATOR);
        this.activity = activity;
        this.twoSide = twoSide;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (twoSide) {
            return new RecipeViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recipe, viewGroup, false));
        }
        else{
            return new RecipeViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recipe_single, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeViewHolder recipeViewHolder, int position) {
        recipeViewHolder.bind(getItem(position));
    }

    /**
     * The type Recipe view holder.
     */
    public class RecipeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        /**
         * The Recipe image.
         */
        ImageView recipeImage, /**
         * The Error image.
         */
        errorImage;
        /**
         * The Recipe rating.
         */
        RatingBar recipeRating;
        /**
         * The Loading image.
         */
        ProgressBar loadingImage;
        /**
         * The Recipe name.
         */
        TextView recipeName, /**
         * The Recipe description.
         */
        recipeDescription, /**
         * The Recipe category.
         */
        recipeCategory, /**
         * The Recipe time.
         */
        recipeTime, /**
         * The Recipe difficulty.
         */
        recipeDifficulty;

        /**
         * Instantiates a new Recipe view holder.
         *
         * @param view the view
         */
        RecipeViewHolder(@NonNull View view) {
            super(view);

            if (twoSide) {
                doViewAdjustSize(view);
            }

            recipeName = view.findViewById(R.id.recipe_name);
            recipeDescription = view.findViewById(R.id.recipe_description);
            recipeCategory = view.findViewById(R.id.recipe_category);
            recipeTime = view.findViewById(R.id.recipe_time);
            recipeDifficulty = view.findViewById(R.id.recipe_difficulty);
            recipeImage = view.findViewById(R.id.recipe_image);
            recipeRating = view.findViewById(R.id.recipe_rating);
            errorImage = view.findViewById(R.id.error_image);
            loadingImage = view.findViewById(R.id.image_loading);

            view.setOnClickListener(this);
        }

        /**
         * Bind.
         *
         * @param recipe the recipe
         */
        void bind(Recipe recipe) {
            recipeName.setText(recipe.getName());
            recipeDescription.setText(recipe.getDescription());
            String duration = recipe.getDuration() + " mins";
            recipeTime.setText(duration);
            recipeDifficulty.setText(recipe.getDifficulty());

            if (recipe.getCategory() != null) {
                recipeCategory.setText(recipe.getCategory().getName());
            }

            if (recipe.getRecipeImages() != null) {
                if (recipe.getRecipeImages().size() > 0) {

                    GlideApp.with(activity)
                            .load(recipe.getRecipeImages().get(0).getImage())
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .transition(withCrossFade())
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    loadingImage.setVisibility(View.GONE);
                                    errorImage.setVisibility(View.VISIBLE);
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    loadingImage.setVisibility(View.GONE);
                                    errorImage.setVisibility(View.GONE);
                                    return false;
                                }
                            })
                            .into(recipeImage);


                } else {
                    loadingImage.setVisibility(View.GONE);
                    errorImage.setVisibility(View.VISIBLE);
                    recipeImage.setImageDrawable(null);
                }
            }
        }

        @Override
        public void onClick(View v) {
            listener.onItemClick(getItem(getAdapterPosition()));
        }

        private void doViewAdjustSize(View view){
            //Set the with of the list item to 70% of the phones screen
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;

            double widthPercentage = width * 0.70;

            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.width = ((int) widthPercentage);
            view.setLayoutParams(params);
        }
    }

    /**
     * The interface Recycler click listener.
     */
    public interface RecyclerClickListener{
        /**
         * On item click.
         *
         * @param item the item
         */
        void onItemClick(Recipe item);
    }
}
