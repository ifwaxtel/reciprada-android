package com.ifwaxtel.reciprada.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.core.model.RecipeTool;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Orji Ifeanyi C. on 30/06/2018.
 */
public class RecipeToolAdapter extends RecyclerView.Adapter<RecipeToolAdapter.ViewHolder>{
    private LayoutInflater mInflater;
    private List<RecipeTool> mResults;
    private RecyclerClickListener recyclerClickListener;
    private Context context;

    /**
     * Instantiates a new Recipe tool adapter.
     *
     * @param context  the context
     * @param listener the listener
     */
    public RecipeToolAdapter(Context context, RecyclerClickListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mResults = new ArrayList<>();
        this.context = context;
        this.recyclerClickListener = listener;
    }

    /**
     * Add tools.
     *
     * @param _recipeTools the recipe tools
     */
    public void addTools(List<RecipeTool> _recipeTools){
        mResults.clear();
        mResults.addAll(_recipeTools);
        this.notifyDataSetChanged();
    }

    /**
     * Instantiates a new Recipe tool adapter.
     */
    public RecipeToolAdapter(){

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(mInflater.inflate(R.layout.item_recipe_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final RecipeTool recipeTool = mResults.get(position);
        if(recipeTool.getTool() != null) {
            holder.itemName.setText(recipeTool.getTool().getName());
            holder.itemInfo.setVisibility(View.GONE);

            if (recipeTool.getTool().getImage() != null) {
                GlideApp.with(context)
                        .load(recipeTool.getTool().getImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transition(withCrossFade())
                        .into(holder.itemImage);
            }
        }

    }

    private RecipeTool getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public int getItemCount() {
        return mResults.size();
    }

    /**
     * The type View holder.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Item name.
         */
        TextView itemName, /**
         * The Item info.
         */
        itemInfo;
        /**
         * The Item image.
         */
        ImageView itemImage;

        /**
         * Instantiates a new View holder.
         *
         * @param view the view
         */
        ViewHolder(View view) {
            super(view);
            itemName = view.findViewById(R.id.item_name);
            itemInfo = view.findViewById(R.id.item_info);
            itemImage = view.findViewById(R.id.item_image);

            view.setOnClickListener(v -> {
                final int position = getAdapterPosition();
                recyclerClickListener.onItemClick(getItem(position), view);
            });
        }
    }

    /**
     * The interface Recycler click listener.
     */
    public interface RecyclerClickListener{
        /**
         * On item click.
         *
         * @param item the item
         * @param view the view
         */
        void onItemClick(RecipeTool item, View view);
    }
}
