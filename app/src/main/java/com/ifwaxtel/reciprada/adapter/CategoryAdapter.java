package com.ifwaxtel.reciprada.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ifwaxtel.reciprada.GlideApp;
import com.ifwaxtel.reciprada.R;
import com.ifwaxtel.reciprada.core.model.Category;

import java.util.ArrayList;
import java.util.List;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

/**
 * Created by Orji Ifeanyi C. on 30/06/2018.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> implements Filterable{
    private LayoutInflater mInflater;
    private List<Category> mResults;
    private List<Category> mResultsFiltered;
    private RecyclerClickListener recyclerClickListener;
    private Activity context;
    private View emptyView;

    /**
     * Instantiates a new Category adapter.
     *
     * @param context  the context
     * @param listener the listener
     */
    public CategoryAdapter(Activity context, RecyclerClickListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.recyclerClickListener = listener;
        this.mResults = new ArrayList<>();
        this.mResultsFiltered = new ArrayList<>();
        registerAdapterDataObserver(emptyObserver);
    }

    /**
     * Sets empty view.
     *
     * @param emptyView the empty view
     */
    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
    }

    /**
     * Add categories.
     *
     * @param _categories the categories
     */
    public void addCategories(List<Category> _categories){
        if (mResults.size() != 0) {
            for (Category category : _categories) {
                if (mResults.contains(category)) {
                    //mResults.indexOf(recipe);
                    mResults.set(mResults.indexOf(category), category);
                    Log.w("Recipes", "replaced " + category.getName());
                }
                else{
                    mResults.add(category);
                    Log.w("Recipes", "added " + category.getName());
                }
            }
        }
        else {
            mResults.addAll(_categories);
        }

        mResultsFiltered = mResults;
        this.notifyDataSetChanged();
    }

    /**
     * Add more recipes.
     *
     * @param _categories the categories
     */
    public void addMoreRecipes(List<Category> _categories){
        mResults.addAll(_categories);
        mResultsFiltered = mResults;
        this.notifyItemRangeInserted(mResults.size(), _categories.size());
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryAdapter.ViewHolder(mInflater.inflate(R.layout.item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        final Category category = mResults.get(position);
        holder.categoryName.setText(category.getName());
        if(category.getImage() != null){
            GlideApp.with(context)
                    .load(category.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .transition(withCrossFade())
                    .into(holder.categoryImage);
        }
    }

    private Category getItem(int position) {
        return mResults.get(position);
    }

    @Override
    public int getItemCount() {
        return mResultsFiltered.size();
    }

    /**
     * Get last item id int.
     *
     * @return the int
     */
    public int getLastItemId(){
        if (mResults != null && mResults.size() != 0){
            return getItem(mResults.size() - 1).getId();
        }
        else{
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if(charString.isEmpty()){
                    mResultsFiltered = mResults;
                }else{
                    List<Category> filteredList = new ArrayList<>();
                    for(Category category : mResults){
                        if(category.getName().toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(category);
                        }
                    }

                    mResultsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mResultsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mResultsFiltered = (ArrayList<Category>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    /**
     * The type View holder.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Category name.
         */
        TextView categoryName;
        /**
         * The Category image.
         */
        ImageView categoryImage;
        private ViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.category_name);
            categoryImage = view.findViewById(R.id.category_image);


            view.setOnClickListener(v -> {
                final int position = getAdapterPosition();
                recyclerClickListener.onItemClick(getItem(position));
            });
        }
    }

    /**
     * The interface Recycler click listener.
     */
    public interface RecyclerClickListener{
        /**
         * On item click.
         *
         * @param item the item
         */
        void onItemClick(Category item);
    }


    private RecyclerView.AdapterDataObserver emptyObserver = new RecyclerView.AdapterDataObserver() {


        @Override
        public void onChanged() {
            RecyclerView.Adapter<?> adapter = CategoryAdapter.this;
            if (emptyView != null) {
                if (adapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    //recyclerView.setVisibility(View.GONE);
                } else {
                    emptyView.setVisibility(View.GONE);
                    //recyclerView.setVisibility(View.VISIBLE);
                }
            }

        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);

            RecyclerView.Adapter<?> adapter = CategoryAdapter.this;
            if (emptyView != null) {
                if (adapter.getItemCount() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                    //recyclerView.setVisibility(View.GONE);
                } else {
                    emptyView.setVisibility(View.GONE);
                    //recyclerView.setVisibility(View.VISIBLE);
                }
            }
        }
    };
}
