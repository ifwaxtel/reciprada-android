package com.ifwaxtel.reciprada.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;

import com.ifwaxtel.reciprada.database.RecipradaDatabase;
import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.core.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The type Categories repository.
 */
public class CategoriesRepository implements RepositoryListener.CategoriesRetrieve {

    private static CategoriesRepository cInstance;
    private RecipradaDatabase database;
    private ExecutorService executorService;

    /**
     * Gets instance.
     *
     * @param application the application
     * @return the instance
     */
    public static CategoriesRepository getInstance(Application application) {
        if (cInstance == null) {
            cInstance = new CategoriesRepository(application);
        }
        return cInstance;
    }

    private CategoriesRepository(Application application) {
        this.database = RecipradaDatabase.getDatabase(application);
        this.executorService = Executors.newSingleThreadExecutor();
    }

    /**
     * Load categories live data.
     *
     * @return the live data
     */
    public LiveData<List<Category>> loadCategories(){
        return database.categoryDao().loadCategories();
    }

    /**
     * Load home categories live data.
     *
     * @return the live data
     */
    public LiveData<List<Category>> loadHomeCategories(){
        return database.categoryDao().loadhomeCategories();
    }

    /**
     * Load more categories list.
     *
     * @param lastCategoryId the last category id
     * @return the list
     */
    public List<Category> loadMoreCategories(int lastCategoryId){

        Callable<List<Category>> callable = () -> database.categoryDao().loadMoreCategories(lastCategoryId);

        List<Category> rec = new ArrayList<>();
        try {
            rec.addAll(executorService.submit(callable).get());
        } catch (InterruptedException e) {
            //e.printStackTrace();
        } catch (ExecutionException e) {
            //e.printStackTrace();
        }
        return rec;
    }

    @Override
    public void onFetchCategoriesSuccess() {

    }

    @Override
    public void onFetchFailed(String response) {

    }
}
