package com.ifwaxtel.reciprada.repository.callback;

import androidx.paging.PagedList;
import android.content.Context;
import androidx.annotation.NonNull;

import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.service.BackgroundTaskService;

/**
 * The type Recipe boundary callback.
 */
public class RecipeBoundaryCallback extends PagedList.BoundaryCallback<Recipe> {
    private Context context;
    private int categoryId;
    private String token;

    /**
     * Instantiates a new Recipe boundary callback.
     *
     * @param context     the context
     * @param token       the token
     * @param category_id the category id
     */
    public RecipeBoundaryCallback(Context context, String token, int category_id) {
        this.context = context;
        this.categoryId = category_id;
        this.token = token;
    }

    private void loadDataFromBackend(){
        if (categoryId == 0){
            BackgroundTaskService.startActionFetchMoreRecipes(context, token);
        }
        else {
            BackgroundTaskService.startActionFetchMoreRecipesByCategory(context, token, categoryId);
        }
    }

    @Override
    public void onZeroItemsLoaded() {
        loadDataFromBackend();
    }

    /*@Override
    public void onItemAtFrontLoaded(@NonNull Recipe itemAtFront) {
        super.onItemAtFrontLoaded(itemAtFront);
    }*/

    @Override
    public void onItemAtEndLoaded(@NonNull Recipe itemAtEnd) {
        loadDataFromBackend();
    }
}
