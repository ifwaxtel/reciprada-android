package com.ifwaxtel.reciprada.repository;

import android.app.Application;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;
import android.util.Log;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.database.RecipradaDatabase;
import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Ingredient;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.core.model.RecipeIngredient;
import com.ifwaxtel.reciprada.core.model.RecipeStep;
import com.ifwaxtel.reciprada.core.model.RecipeTool;
import com.ifwaxtel.reciprada.core.model.Tool;
import com.ifwaxtel.reciprada.remote.request.ApiClient;
import com.ifwaxtel.reciprada.remote.request.ApiService;
import com.ifwaxtel.reciprada.remote.response.CategoryListResponse;
import com.ifwaxtel.reciprada.remote.response.IngredientListResponse;
import com.ifwaxtel.reciprada.remote.response.RecipeDetailsResponse;
import com.ifwaxtel.reciprada.remote.response.RecipeListResponse;
import com.ifwaxtel.reciprada.remote.response.TokenResponse;
import com.ifwaxtel.reciprada.remote.response.ToolListResponse;
import com.ifwaxtel.reciprada.utils.PrefUtil;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * The type Remote repository.
 */
public class RemoteRepository {
    private Executor executor;
    private RecipradaDatabase database;

    /**
     * Instantiates a new Remote repository.
     *
     * @param application the application
     */
    public RemoteRepository(Application application) {
        this.executor = Executors.newSingleThreadExecutor();
        this.database = RecipradaDatabase.getDatabase(application);
    }

    /**
     * Fetch token.
     *
     * @param prefUtil the pref util
     * @param listener the listener
     */
    public static void fetchToken(PrefUtil prefUtil, RepositoryListener.TokenRetrieve listener){
        ApiClient.getClient().create(ApiService.class).getApiToken(GlobalConstants.RECIPRADA_API_KEY).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(@NonNull Call<TokenResponse> call, @NonNull Response<TokenResponse> response) {
                if (response.isSuccessful()) {
                    SharedPreferences.Editor editor = prefUtil.getEditor();
                    prefUtil.setToken(editor, Objects.requireNonNull(response.body()).getToken());
                    prefUtil.commitEdit(editor);

                    listener.onFetchTokenSuccess(Objects.requireNonNull(response.body()).getToken());
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<TokenResponse> call, @NonNull Throwable t) {
                Log.w("Token", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

    /**
     * Load categories from api.
     *
     * @param request_token the request token
     * @param listener      the listener
     * @param page          the page
     */
    public void loadCategoriesFromApi(String request_token, RepositoryListener.CategoriesRetrieve listener, int page){
        ApiClient.getClient().create(ApiService.class).loadCategories(request_token, page).enqueue(new Callback<CategoryListResponse>() {
            @Override
            public void onResponse(@NonNull Call<CategoryListResponse> call, @NonNull Response<CategoryListResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        for(Category category : Objects.requireNonNull(response.body()).getCategories()) {
                            database.categoryDao().insertCategory(category);
                        }
                    });

                    listener.onFetchCategoriesSuccess();
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<CategoryListResponse> call, @NonNull Throwable t) {
                Log.w("Category", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

    /**
     * Load recipes from api.
     *
     * @param request_token the request token
     * @param listener      the listener
     * @param page          the page
     */
    public void loadRecipesFromApi(String request_token, RepositoryListener.RecipesRetrieve listener, int page){
        ApiClient.getClient().create(ApiService.class).loadRecipes(request_token, page).enqueue(new Callback<RecipeListResponse>() {
            @Override
            public void onResponse(@NonNull Call<RecipeListResponse> call, @NonNull Response<RecipeListResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        for(Recipe recipe : Objects.requireNonNull(response.body()).getRecipes()) {
                            recipe.setLoadedFrom(GlobalConstants.LOADED_FROM_RECIPE_LIST);
                            database.recipeDao().insertRecipe(recipe);

                            for(RecipeImage recipeImage : recipe.getRecipeImages()){
                                database.recipeImageDao().insertRecipeImage(recipeImage);
                            }
                        }
                    });
                    listener.onFetchRecipesSuccess();
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RecipeListResponse> call, @NonNull Throwable t) {
                Log.w("Recipe", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

    /**
     * Load recipes by category from api.
     *
     * @param request_token the request token
     * @param listener      the listener
     * @param page          the page
     * @param categoryId    the category id
     */
    public void loadRecipesByCategoryFromApi(String request_token, RepositoryListener.RecipesRetrieve listener,  int page, int categoryId){
        ApiClient.getClient().create(ApiService.class).loadRecipesByCategory(request_token, categoryId, page).enqueue(new Callback<RecipeListResponse>() {
            @Override
            public void onResponse(@NonNull Call<RecipeListResponse> call, @NonNull Response<RecipeListResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        for(Recipe recipe : Objects.requireNonNull(response.body()).getRecipes()) {
                            recipe.setLoadedFrom(GlobalConstants.LOADED_FROM_RECIPE_CATEGORY);
                            database.recipeDao().insertRecipe(recipe);

                            for(RecipeImage recipeImage : recipe.getRecipeImages()){
                                database.recipeImageDao().insertRecipeImage(recipeImage);
                            }
                        }
                    });
                    listener.onFetchRecipesSuccess();
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RecipeListResponse> call, @NonNull Throwable t) {
                Log.w("Recipe", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

    /**
     * Load recipe details from api.
     *
     * @param request_token the request token
     * @param id            the id
     */
    public void loadRecipeDetailsFromApi(String request_token, int id){
        ApiClient.getClient().create(ApiService.class).loadRecipeDetails(request_token, id).enqueue(new Callback<RecipeDetailsResponse>() {
            @Override
            public void onResponse(@NonNull Call<RecipeDetailsResponse> call, @NonNull Response<RecipeDetailsResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        Recipe recipe = Objects.requireNonNull(response.body()).getRecipe();
                        recipe.setDetailsLoaded(true);
                        database.recipeDao().updateRecipe(recipe);
                        int recipeID = recipe.getId();

                        database.categoryDao().insertCategory(recipe.getCategory());

                        for (RecipeImage image : recipe.getRecipeImages()){
                            image.setRecipeId(recipeID);
                            database.recipeImageDao().insertRecipeImage(image);
                        }

                        for (RecipeIngredient ingredient : recipe.getRecipeIngredients()){
                            ingredient.setIngredientId(ingredient.getIngredient().getId());
                            ingredient.setRecipeId(recipeID);
                            database.ingredientDao().insertIngredient(ingredient.getIngredient());
                            database.recipeIngredientDao().insertRecipeIngredient(ingredient);
                        }

                        for (RecipeStep step : recipe.getRecipeSteps()){
                            step.setRecipeId(recipeID);
                            database.recipeStepDao().insertRecipeStep(step);
                        }

                        for (RecipeTool tool : recipe.getRecipeTools()){
                            tool.setRecipeId(recipeID);
                            tool.setToolId(tool.getTool().getId());
                            database.toolDao().insertTool(tool.getTool());
                            database.recipeToolDao().insertRecipeTool(tool);
                        }

                    });
                }
            }

            @Override
            public void onFailure(@NonNull Call<RecipeDetailsResponse> call, @NonNull Throwable t) {
                Log.w("RecipeDetails", "fetch failed : " + t.getMessage());
            }
        });
    }

    /**
     * Load ingredients from api.
     *
     * @param request_token the request token
     * @param listener      the listener
     */
    public void loadIngredientsFromApi(String request_token, RepositoryListener.IngredientsRetrieve listener){
        ApiClient.getClient().create(ApiService.class).loadIngredients(request_token).enqueue(new Callback<IngredientListResponse>() {
            @Override
            public void onResponse(@NonNull Call<IngredientListResponse> call, @NonNull Response<IngredientListResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        for(Ingredient ingredient : Objects.requireNonNull(response.body()).getIngrediets()) {
                            database.ingredientDao().insertIngredient(ingredient);
                        }
                    });

                    listener.onFetchIngredientsSuccess();
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IngredientListResponse> call, @NonNull Throwable t) {
                Log.w("Ingredient", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

    /**
     * Load tools from api.
     *
     * @param request_token the request token
     * @param listener      the listener
     */
    public void loadToolsFromApi(String request_token, RepositoryListener.ToolsRetrieve listener){
        ApiClient.getClient().create(ApiService.class).loadTools(request_token).enqueue(new Callback<ToolListResponse>() {
            @Override
            public void onResponse(@NonNull Call<ToolListResponse> call, @NonNull Response<ToolListResponse> response) {
                if (response.isSuccessful()){
                    executor.execute(() -> {
                        for(Tool tool : Objects.requireNonNull(response.body()).getTools()) {
                            database.toolDao().insertTool(tool);
                        }
                    });

                    listener.onFetchToolsSuccess();
                }
                else{
                    try {
                        listener.onFetchFailed(Objects.requireNonNull(response.errorBody()).string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ToolListResponse> call, @NonNull Throwable t) {
                Log.w("Tool", "fetch failed : " + t.getMessage());
                listener.onFetchFailed(t.getMessage());
            }
        });
    }

}
