package com.ifwaxtel.reciprada.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.database.RecipradaDatabase;
import com.ifwaxtel.reciprada.listeners.RepositoryListener;
import com.ifwaxtel.reciprada.core.model.Category;
import com.ifwaxtel.reciprada.core.model.Ingredient;
import com.ifwaxtel.reciprada.core.model.Recipe;
import com.ifwaxtel.reciprada.core.model.RecipeImage;
import com.ifwaxtel.reciprada.core.model.RecipeIngredient;
import com.ifwaxtel.reciprada.core.model.RecipeStep;
import com.ifwaxtel.reciprada.core.model.RecipeTool;
import com.ifwaxtel.reciprada.core.model.Tool;
import com.ifwaxtel.reciprada.repository.callback.RecipeBoundaryCallback;
import com.ifwaxtel.reciprada.utils.PrefUtil;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * The type Recipes repository.
 */
public class RecipesRepository implements RepositoryListener.RecipesRetrieve, RepositoryListener.CategoriesRetrieve {

    private static RecipesRepository rInstance;
    private Executor executor;
    private RecipradaDatabase database;
    private String token;
    private Application application;

    /**
     * Gets instance.
     *
     * @param application the application
     * @return the instance
     */
    public static RecipesRepository getInstance(Application application) {
        if (rInstance == null) {
            rInstance = new RecipesRepository(application);
        }
        return rInstance;
    }

    private RecipesRepository(Application application) {
        this.application = application;
        this.database = RecipradaDatabase.getDatabase(application);
        this.executor = Executors.newSingleThreadExecutor();
        this.token = "Bearer " + new PrefUtil(application.getApplicationContext()).getToken();


    }

    /**
     * Load recipes live data.
     *
     * @return the live data
     */
    public LiveData<List<Recipe>> loadRecipes(){
        return database.recipeDao().loadRecipes();
    }

    /**
     * Load paged recipes live data.
     *
     * @return the live data
     */
    public LiveData<PagedList<Recipe>> loadPagedRecipes(){
        // Get data source factory from the local cache
        DataSource.Factory<Integer, Recipe> recipesSource = database.recipeDao().loadRecipesSource();

        // Construct the boundary callback
        RecipeBoundaryCallback boundaryCallback = new RecipeBoundaryCallback(application, token, 0);

        // Set the Page size for the Paged list
        PagedList.Config pagedConfig = new PagedList.Config.Builder()
                .setPageSize(GlobalConstants.DATABASE_PAGE_SIZE)
                .build();

        return new LivePagedListBuilder<>(recipesSource, pagedConfig)
                .setBoundaryCallback(boundaryCallback)
                .build();
    }

    /**
     * Load recipes by category live data.
     *
     * @param categoryId the category id
     * @return the live data
     */
    public LiveData<PagedList<Recipe>> loadRecipesByCategory(int categoryId){

        //BackgroundTaskService.startActionFetchRecipesByCategory(application, token, categoryId);
        //return database.recipeDao().loadRecipesByCategory(categoryId);

        // Get data source factory from the local cache
        DataSource.Factory<Integer, Recipe> recipesSource = database.recipeDao().loadRecipesByCategory(categoryId);

        // Construct the boundary callback
        RecipeBoundaryCallback boundaryCallback = new RecipeBoundaryCallback(application, token, categoryId);

        // Set the Page size for the Paged list
        PagedList.Config pagedConfig = new PagedList.Config.Builder()
                .setPageSize(GlobalConstants.DATABASE_PAGE_SIZE)
                .build();

        return new LivePagedListBuilder<>(recipesSource, pagedConfig)
                .setBoundaryCallback(boundaryCallback)
                .build();
    }

    /**
     * Load live favourite recipes live data.
     *
     * @return the live data
     */
    public LiveData<List<Recipe>> loadLiveFavouriteRecipes(){
        return database.recipeDao().loadLiveFavouriteRecipes();
    }

    /**
     * Load favourite recipes live data.
     *
     * @return the live data
     */
    public LiveData<PagedList<Recipe>> loadFavouriteRecipes(){
        // Get data source factory from the local cache
        DataSource.Factory<Integer, Recipe> recipesSource = database.recipeDao().loadFavouriteRecipes();

        // Construct the boundary callback
        RecipeBoundaryCallback boundaryCallback = new RecipeBoundaryCallback(application, token, 0);

        // Set the Page size for the Paged list
        PagedList.Config pagedConfig = new PagedList.Config.Builder()
                .setPageSize(GlobalConstants.DATABASE_PAGE_SIZE)
                .build();

        return new LivePagedListBuilder<>(recipesSource, pagedConfig)
                .setBoundaryCallback(boundaryCallback)
                .build();
    }

    /**
     * Load recipe images list.
     *
     * @param recipeId the recipe id
     * @return the list
     */
    public List<RecipeImage> loadRecipeImages(int recipeId){
        return database.recipeImageDao().loadImagesForRecipe(recipeId);
    }

    /**
     * Load recipe category category.
     *
     * @param recipeId the recipe id
     * @return the category
     */
    public Category loadRecipeCategory(int recipeId){
        return database.categoryDao().loadRecipeCategory(recipeId);
    }

    /**
     * Load recipe ingredients live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    public LiveData<List<RecipeIngredient>> loadRecipeIngredients(int recipeId){

        return database.recipeIngredientDao().loadRecipeIngredients(recipeId);
    }

    /**
     * Load ingredient byid ingredient.
     *
     * @param ingredientId the ingredient id
     * @return the ingredient
     */
    public Ingredient loadIngredientByid(int ingredientId){
        return database.ingredientDao().loadIngredient(ingredientId);
    }

    /**
     * Load tool byid tool.
     *
     * @param toolId the tool id
     * @return the tool
     */
    public Tool loadToolByid(int toolId){
        return database.toolDao().loadTool(toolId);
    }

    /**
     * Load recipe tools live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    public LiveData<List<RecipeTool>> loadRecipeTools(int recipeId){

        return database.recipeToolDao().loadRecipeTools(recipeId);
    }

    /**
     * Load recipe steps live data.
     *
     * @param recipeId the recipe id
     * @return the live data
     */
    public LiveData<List<RecipeStep>> loadRecipeSteps(int recipeId){
        return database.recipeStepDao().loadRecipeSteps(recipeId);
    }

    /**
     * Update recipe.
     *
     * @param recipe the recipe
     */
    public void updateRecipe(Recipe recipe){
        executor.execute(() -> database.recipeDao().updateRecipe(recipe));
    }

    @Override
    public void onFetchRecipesSuccess() {

    }

    @Override
    public void onFetchCategoriesSuccess() {

    }

    @Override
    public void onFetchFailed(String response) {

    }
}
