package com.ifwaxtel.reciprada.utils;

import androidx.test.espresso.IdlingResource;

import static androidx.test.espresso.Espresso.registerIdlingResources;
import static androidx.test.espresso.Espresso.unregisterIdlingResources;

/**
 * Created by Mahmoud Abdurrahman (mahmoud.abdurrahman@crossover.com) on 2/14/18.
 */
public class WaitUtils {

    private static final int DEFAULT_WAIT_TIME = 3000;

    private static IdlingResource idlingResource;

    public static void waitTime() {
        waitTime(DEFAULT_WAIT_TIME);
    }

    public static void waitTime(int waitingTime) {
        cleanupWaitTime();

        idlingResource = new ElapsedTimeIdlingResource(waitingTime);
        registerIdlingResources(idlingResource);
    }

    public static void cleanupWaitTime() {
        if (idlingResource == null) return;

        unregisterIdlingResources(idlingResource);
        idlingResource = null;
    }
}
