package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Tool test.
 */
public class ToolTest {

    private static Tool tool;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        tool = new Tool();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong value returned by describeContents", 0, tool.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        tool.setId(1);
        assertEquals("wrong value returned by getId", 1, tool.getId());
    }

    /**
     * Gets name.
     */
    @Test
    public void getName() {
        tool.setName("test tool");
        assertEquals("wrong value returned by getName", "test tool", tool.getName());
    }

    /**
     * Gets description.
     */
    @Test
    public void getDescription() {
        tool.setDescription("this is a test description");
        assertEquals("wrong value returned by getDescription", "this is a test description", tool.getDescription());
    }

    /**
     * Gets image.
     */
    @Test
    public void getImage() {
        tool.setImage("http://reciprada.com/image");
        assertEquals("wrong value returned by getImage", "http://reciprada.com/image", tool.getImage());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        Tool tool = new Tool();
        tool.setId(6);
        tool.setName("test tool 2");
        tool.setDescription("this is a test description too");
        tool.setImage("http://reciprada.com/image2");

        String tReturn = "Tool{" +
                "id=6" +
                ", name='test tool 2'" +
                ", description='this is a test description too'" +
                ", image='http://reciprada.com/image2'" +
                '}';

        assertEquals("Invalid toString returned", tReturn, tool.toString());
    }
}