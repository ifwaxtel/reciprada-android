package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Recipe step test.
 */
public class RecipeStepTest {

    private static RecipeStep recipeStep;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        recipeStep = new RecipeStep();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong value returned by describeContents", 0, recipeStep.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        recipeStep.setId(1);
        assertEquals("wrong value returned by getId", 1, recipeStep.getId());
    }

    /**
     * Gets recipe id.
     */
    @Test
    public void getRecipeId() {
        recipeStep.setRecipeId(3);
        assertEquals("wrong value returned by getRecipeId", 3, recipeStep.getRecipeId());
    }

    /**
     * Gets image.
     */
    @Test
    public void getImage() {
        recipeStep.setImage("http://reciprada.com/image");
        assertEquals("wrong value returned by getImage", "http://reciprada.com/image", recipeStep.getImage());
    }

    /**
     * Gets description.
     */
    @Test
    public void getDescription() {
        recipeStep.setDescription("test recipe step description");
        assertEquals("wrong value returned by getDescription", "test recipe step description", recipeStep.getDescription());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        recipeStep.setId(8);
        recipeStep.setRecipeId(4);
        recipeStep.setDescription("test recipe step");
        recipeStep.setImage("http://reciprada.com/image2");

        String rsReturn = "RecipeStep{" +
                "id=8" +
                ", recipeId=4" +
                ", image='http://reciprada.com/image2'" +
                ", description='test recipe step'" +
                '}';

        assertEquals("Invalid toString returned", rsReturn, recipeStep.toString());
    }
}