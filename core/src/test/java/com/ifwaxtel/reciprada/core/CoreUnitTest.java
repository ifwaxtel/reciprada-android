package com.ifwaxtel.reciprada.core;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;
import com.ifwaxtel.reciprada.core.model.CategoryTest;
import com.ifwaxtel.reciprada.core.model.IngredientTest;
import com.ifwaxtel.reciprada.core.model.RecipeImageTest;
import com.ifwaxtel.reciprada.core.model.RecipeIngredientTest;
import com.ifwaxtel.reciprada.core.model.RecipeStepTest;
import com.ifwaxtel.reciprada.core.model.RecipeTest;
import com.ifwaxtel.reciprada.core.model.RecipeToolTest;
import com.ifwaxtel.reciprada.core.model.ToolTest;
import com.ifwaxtel.reciprada.core.utils.RandomUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static org.junit.Assert.*;

/**
 * The type Core unit test.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CoreUnitTest.RandomUtilTest.class,
        CoreUnitTest.GlobalConstantTest.class,
        CategoryTest.class,
        IngredientTest.class,
        ToolTest.class,
        RecipeTest.class,
        RecipeImageTest.class,
        RecipeIngredientTest.class,
        RecipeStepTest.class,
        RecipeToolTest.class,
})
public class CoreUnitTest {

    /**
     * The type Random util test.
     */
    public static class RandomUtilTest {
        /**
         * Test random util length.
         */
        @Test
        public void testRandomUtilLength() {
            //Test that RandomUtil is generating a random password with 10 characters
            assertEquals("Wrong password length returned", 10, RandomUtil.randomPassword(10).length());
        }
    }

    /**
     * The type Global constant test.
     */
    public static class GlobalConstantTest {
        /**
         * Test global constant item retrieve.
         */
        @Test
        public void testGlobalConstantItemRetrieve(){
            assertNotNull("Null response from GlobalConstants", GlobalConstants.APP_NAME);
        }
    }

}