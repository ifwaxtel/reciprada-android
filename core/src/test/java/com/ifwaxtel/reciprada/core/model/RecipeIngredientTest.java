package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Recipe ingredient test.
 */
public class RecipeIngredientTest {

    private static RecipeIngredient recipeIngredient;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        recipeIngredient = new RecipeIngredient();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong value returned by describeContents", 0, recipeIngredient.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        recipeIngredient.setId(1);
        assertEquals("wrong value returned by getId", 1, recipeIngredient.getId());
    }

    /**
     * Gets recipe id.
     */
    @Test
    public void getRecipeId() {
        recipeIngredient.setRecipeId(3);
        assertEquals("wrong value returned by getRecipeId", 3, recipeIngredient.getRecipeId());
    }

    /**
     * Gets ingredient id.
     */
    @Test
    public void getIngredientId() {
        recipeIngredient.setIngredientId(5);
        assertEquals("wrong value returned by getIngredientId", 5, recipeIngredient.getIngredientId());
    }

    /**
     * Gets ingredient.
     */
    @Test
    public void getIngredient() {
        Ingredient ingredient = new Ingredient();
        ingredient.setId(12);
        recipeIngredient.setIngredient(ingredient);

        assertEquals("wrong value returned by getIngredient", ingredient, recipeIngredient.getIngredient());
    }

    /**
     * Gets quantity.
     */
    @Test
    public void getQuantity() {
        recipeIngredient.setQuantity(30);
        assertEquals("wrong value returned by getQuantity", 30, recipeIngredient.getQuantity(), 0);
    }

    /**
     * Gets quantity scale.
     */
    @Test
    public void getQuantityScale() {
        recipeIngredient.setQuantityScale("ltr");
        assertEquals("wrong value returned by getQuantityScale", "ltr", recipeIngredient.getQuantityScale());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        recipeIngredient.setId(9);
        recipeIngredient.setRecipeId(3);
        recipeIngredient.setIngredientId(2);
        recipeIngredient.setQuantity(2);
        recipeIngredient.setQuantityScale("m");

        String riReturn = "RecipeIngredient{" +
                "id=9" +
                ", recipeId=3" +
                ", ingredientId=2" +
                ", ingredient=null" +
                ", quantity=2.0" +
                ", quantityScale='m'" +
                '}';

        assertEquals("Invalid toString returned", riReturn, recipeIngredient.toString());
    }
}