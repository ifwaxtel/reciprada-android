package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Recipe image test.
 */
public class RecipeImageTest {

    private static RecipeImage recipeImage;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        recipeImage = new RecipeImage();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong value returned by describeContents", 0, recipeImage.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        recipeImage.setId(1);
        assertEquals("wrong value returned by getId", 1, recipeImage.getId());
    }

    /**
     * Gets recipe id.
     */
    @Test
    public void getRecipeId() {
        recipeImage.setRecipeId(3);
        assertEquals("wrong value returned by getRecipeId", 3, recipeImage.getRecipeId());
    }

    /**
     * Gets image.
     */
    @Test
    public void getImage() {
        recipeImage.setImage("http://reciprada.com/image");
        assertEquals("wrong value returned by getImage", "http://reciprada.com/image", recipeImage.getImage());
    }

    /**
     * Gets caption.
     */
    @Test
    public void getCaption() {
        recipeImage.setCaption("test caption");
        assertEquals("wrong value returned by getCaption", "test caption", recipeImage.getCaption());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        recipeImage.setId(5);
        recipeImage.setRecipeId(4);
        recipeImage.setImage("http://reciprada.com/image2");
        recipeImage.setCaption("test caption 2");

        String riReturn = "RecipeImage{" +
                "id=5"+
                ", recipeId=4"+
                ", image='http://reciprada.com/image2'" +
                ", caption='test caption 2'" +
                '}';
        assertEquals("Invalid toString returned",riReturn, recipeImage.toString());

    }
}