package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Ingredient test.
 */
public class IngredientTest {

    private static Ingredient ingredient;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        ingredient = new Ingredient();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong describeContents returned", 0, ingredient.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        ingredient.setId(1);
        assertEquals("wrong id returned from getId", 1, ingredient.getId());
    }

    /**
     * Gets name.
     */
    @Test
    public void getName() {
        ingredient.setName("test ingredient");
        assertEquals("wrong value returned from getName", "test ingredient", ingredient.getName());
    }

    /**
     * Gets description.
     */
    @Test
    public void getDescription() {
        ingredient.setDescription("this is a test description");
        assertEquals("wrong value returned from getDescription", "this is a test description", ingredient.getDescription());
    }

    /**
     * Gets image.
     */
    @Test
    public void getImage() {
        ingredient.setImage("http://reciprada.com/image");
        assertEquals("wrong value returned from getImage", "http://reciprada.com/image", ingredient.getImage());
    }


    /**
     * Gets calorie count.
     */
    @Test
    public void getCalorieCount() {
        ingredient.setCalorieCount(2);
        assertEquals("wrong value returned from getCalorieCount", 2, ingredient.getCalorieCount(),0);
    }

    /**
     * Gets protein count.
     */
    @Test
    public void getProteinCount() {
        ingredient.setProteinCount(1);
        assertEquals("wrong value returned from getProteinCount", 1, ingredient.getProteinCount(), 0);
    }

    /**
     * Gets carbohydrate count.
     */
    @Test
    public void getCarbohydrateCount() {
        ingredient.setProteinCount(5);
        assertEquals("wrong value returned from getCarbohydrateCount", 5, ingredient.getProteinCount(), 0);
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        ingredient.setId(2);
        ingredient.setName("test ingredient 3");
        ingredient.setDescription("this is a test ingredient 3");
        ingredient.setImage("http://reciprada.com/image2");
        ingredient.setCalorieCount(22);
        ingredient.setCarbohydrateCount(55);
        ingredient.setProteinCount(10);
        String ingReturn =  "Ingredient{" +
                "id=2"+
                ", name='test ingredient 3'" +
                ", description='this is a test ingredient 3'" +
                ", image='http://reciprada.com/image2'" +
                ", calorieCount=22.0"+
                ", proteinCount=10.0"+
                ", carbohydrateCount=55.0"+
                '}';
        assertEquals("Invalid ingredient toString returned", ingReturn, ingredient.toString());
    }
}