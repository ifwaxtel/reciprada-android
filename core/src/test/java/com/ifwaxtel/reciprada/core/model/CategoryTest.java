package com.ifwaxtel.reciprada.core.model;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Category test.
 */
public class CategoryTest {

    private static Category category;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        category = new Category();
    }

    /**
     * Test hash code.
     */
    @Test
    public void testHashCode() {
        category.setId(1);
        assertEquals("Wrong hashcode returned", GlobalConstants.CATEGORY_HASH + 1, category.hashCode());
    }

    /**
     * Equals.
     */
    @Test
    public void equals() {
        category.setId(2);

        Category testCategory = new Category();
        testCategory.setId(2);

        assertEquals("Equals method not working", category, testCategory);
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("describeContents method not working",0, category.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        category.setId(3);
        assertEquals("getId returning false value", 3, category.getId());
    }

    /**
     * Sets id.
     */
    @Test
    public void setId() {
        category.setId(4);
        assertEquals("setId not functioning properly", 4, category.getId());
    }

    /**
     * Gets name.
     */
    @Test
    public void getName() {
        category.setName("test category");
        assertEquals("getName not functioning properly", "test category", category.getName());
    }

    /**
     * Sets name.
     */
    @Test
    public void setName() {
        category.setName("test category 2");
        assertEquals("setName not functioning properly", "test category 2", category.getName());
    }

    /**
     * Gets description.
     */
    @Test
    public void getDescription() {
        category.setDescription("test description");
        assertEquals("getDescription not functioning properly", "test description", category.getDescription());
    }

    /**
     * Sets description.
     */
    @Test
    public void setDescription() {
        category.setDescription("test description 2");
        assertEquals("setDescription not functioning properly", "test description 2", category.getDescription());
    }

    /**
     * Gets recipes.
     */
    @Test
    public void getRecipes() {
        category.setRecipes(10);
        assertEquals("getRecipes not functioning properly", 10, category.getRecipes());
    }

    /**
     * Sets recipes.
     */
    @Test
    public void setRecipes() {
        category.setRecipes(11);
        assertEquals("setRecipes not functioning properly", 11, category.getRecipes());
    }

    /**
     * Gets image.
     */
    @Test
    public void getImage() {
        category.setImage("http://reciprada.com/image");
        assertEquals("getImage not functioning properly", "http://reciprada.com/image", category.getImage());
    }

    /**
     * Sets image.
     */
    @Test
    public void setImage() {
        category.setImage("http://reciprada.com/image2");
        assertEquals("setImage not functioning properly", "http://reciprada.com/image2", category.getImage());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        category.setId(1);
        category.setName("test category");
        category.setDescription("this is a test category");
        category.setImage("http://reciprada.com/image");
        category.setRecipes(10);
        String catReturn =  "Category{" +
                "id=1" +
                ", name='test category'" +
                ", description='this is a test category'" +
                ", image='http://reciprada.com/image'" +
                ", recipes=10"+
                '}';
        assertEquals("Invalid category toString returned",catReturn, category.toString());
    }
}