package com.ifwaxtel.reciprada.core.model;

import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * The type Recipe test.
 */
public class RecipeTest {

    private static Recipe recipe;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        recipe = new Recipe();
    }

    /**
     * Test hash code.
     */
    @Test
    public void testHashCode() {
        recipe.setId(1);
        assertEquals("Wrong hashcode returned", GlobalConstants.RECIPE_HASH + 1, recipe.hashCode());
    }

    /**
     * Equals.
     */
    @Test
    public void equals() {
        recipe.setId(2);

        Recipe testRecipe = new Recipe();
        testRecipe.setId(2);

        assertEquals("Equals method not working", recipe, testRecipe);
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("describeContents method not working",0, recipe.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        recipe.setId(3);
        assertEquals("wrong value returned by getId", 3, recipe.getId());
    }

    /**
     * Gets category id.
     */
    @Test
    public void getCategoryId() {
        recipe.setCategoryId(5);
        assertEquals("wrong value returned by getCategoryId", 5, recipe.getCategoryId());
    }

    /**
     * Gets name.
     */
    @Test
    public void getName() {
        recipe.setName("test recipe");
        assertEquals("wrong value returned by getName", "test recipe", recipe.getName());
    }

    /**
     * Gets description.
     */
    @Test
    public void getDescription() {
        recipe.setDescription("test description");
        assertEquals("wrong value returned by getDescription", "test description", recipe.getDescription());
    }

    /**
     * Gets difficulty.
     */
    @Test
    public void getDifficulty() {
        recipe.setDifficulty("Hard");
        assertEquals("wrong value returned by getDifficulty", "Hard", recipe.getDifficulty());
    }

    /**
     * Gets duration.
     */
    @Test
    public void getDuration() {
        recipe.setDuration(10);
        assertEquals("wrong value returned by getDuration", 10, recipe.getDuration());
    }

    /**
     * Gets category.
     */
    @Test
    public void getCategory() {
        Category category = new Category();
        category.setId(1);

        recipe.setCategory(category);
        assertEquals("wrong value returned by getCategory", category, recipe.getCategory());
    }

    /**
     * Gets recipe images.
     */
    @Test
    public void getRecipeImages() {
        ArrayList<RecipeImage> recipeImages = new ArrayList<>();
        RecipeImage recipeImage = new RecipeImage();
        recipeImage.setId(10);
        recipeImages.add(recipeImage);

        recipe.setRecipeImages(recipeImages);
        assertEquals("wrong value returned by getRecipeImages", recipeImages, recipe.getRecipeImages());
    }

    /**
     * Gets recipe ingredients.
     */
    @Test
    public void getRecipeIngredients() {
        ArrayList<RecipeIngredient> recipeIngredients = new ArrayList<>();
        RecipeIngredient recipeIngredient = new RecipeIngredient();
        recipeIngredient.setId(11);
        recipeIngredients.add(recipeIngredient);

        recipe.setRecipeIngredients(recipeIngredients);
        assertEquals("wrong value returned by getRecipeIngredients", recipeIngredients, recipe.getRecipeIngredients());
    }

    /**
     * Gets recipe steps.
     */
    @Test
    public void getRecipeSteps() {
        ArrayList<RecipeStep> recipeSteps = new ArrayList<>();
        RecipeStep recipeStep = new RecipeStep();
        recipeStep.setId(18);
        recipeSteps.add(recipeStep);

        recipe.setRecipeSteps(recipeSteps);
        assertEquals("wrong value returned by getRecipeId", recipeSteps, recipe.getRecipeSteps());
    }

    /**
     * Gets recipe tools.
     */
    @Test
    public void getRecipeTools() {
        ArrayList<RecipeTool> recipeTools = new ArrayList<>();
        RecipeTool recipeTool = new RecipeTool();
        recipeTool.setId(19);
        recipeTools.add(recipeTool);

        recipe.setRecipeTools(recipeTools);
        assertEquals("wrong value returned by getRecipeTools", recipeTools, recipe.getRecipeTools());
    }

    /**
     * Is details loaded.
     */
    @Test
    public void isDetailsLoaded() {
        recipe.setDetailsLoaded(true);
        assertTrue("wrong value returned by isDetailsLoaded", recipe.isDetailsLoaded());
    }

    /**
     * Is favourite.
     */
    @Test
    public void isFavourite() {
        recipe.setFavourite(true);
        assertTrue("wrong value returned by isFavourite", recipe.isFavourite());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        recipe.setId(1);
        recipe.setCategoryId(1);
        recipe.setName("Test recipe 2");
        recipe.setDescription("This is a test recipe too");
        recipe.setDifficulty("Easy");
        recipe.setDuration(10);
        recipe.setDetailsLoaded(true);

        String rReturn = "Recipe{" +
                "id=1" +
                ", categoryId=1" +
                ", name='Test recipe 2'" +
                ", description='This is a test recipe too'" +
                ", difficulty='Easy'" +
                ", duration=10" +
                ", category=null" +
                ", recipeImages=[]" +
                ", recipeIngredients=[]" +
                ", recipeSteps=[]" +
                ", recipeTools=[]" +
                ", detailsLoaded=true" +
                '}';

        assertEquals("Invalid toString returned", rReturn, recipe.toString());
    }
}