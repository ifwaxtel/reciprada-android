package com.ifwaxtel.reciprada.core.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The type Recipe tool test.
 */
public class RecipeToolTest {

    private static RecipeTool recipeTool;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
        recipeTool = new RecipeTool();
    }

    /**
     * Describe contents.
     */
    @Test
    public void describeContents() {
        assertEquals("wrong value returned by describeContents", 0, recipeTool.describeContents());
    }

    /**
     * Gets id.
     */
    @Test
    public void getId() {
        recipeTool.setId(1);
        assertEquals("wrong value returned by getId", 1, recipeTool.getId());
    }

    /**
     * Gets recipe id.
     */
    @Test
    public void getRecipeId() {
        recipeTool.setRecipeId(3);
        assertEquals("wrong value returned by getRecipeId", 3, recipeTool.getRecipeId());
    }

    /**
     * Gets tool id.
     */
    @Test
    public void getToolId() {
        recipeTool.setToolId(11);
        assertEquals("wrong value returned by getToolId", 11, recipeTool.getToolId());
    }

    /**
     * Gets tool.
     */
    @Test
    public void getTool() {
        Tool tool = new Tool();
        tool.setId(12);
        recipeTool.setTool(tool);
        assertEquals("wrong value returned by getTool", tool, recipeTool.getTool());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        recipeTool.setId(10);
        recipeTool.setToolId(18);
        recipeTool.setRecipeId(15);

        String rtReturn = "RecipeTool{" +
                "id=10" +
                ", recipeId=15" +
                ", toolId=18" +
                ", tool=null" +
                '}';

        assertEquals("Invalid toString returned", rtReturn, recipeTool.toString());
    }
}