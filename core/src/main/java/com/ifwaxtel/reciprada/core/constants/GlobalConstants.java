package com.ifwaxtel.reciprada.core.constants;

/**
 * The type Global constants.
 */
//Adding final makes the class non-extendable
public final class GlobalConstants {

    /**
     * The constant APP_NAME.
     */
    public static final String APP_NAME = "Reciprada";
    /**
     * The constant API_KEY.
     */
    public static final String API_KEY = "api_key";
    /**
     * The constant RECIPRADA_API_KEY.
     */
    public static final String RECIPRADA_API_KEY = "0D1vLZ8zCbaQDYpXGV6KZD3c6OyQFi5AAC6csJWjpJrfwTtxzt3L2q37TnQU";
    /**
     * The constant NAME.
     */
    public static final String NAME = "name";

    /**
     * The constant LOGGED_IN.
     */
    public static final String LOGGED_IN = "logged_in";
    /**
     * The constant BASE_URL.
     */
    public static final String BASE_URL = "https://reciprada.com/api/v1/";
    //public static final String BASE_URL = "http://192.168.20.143/reciprada/public/api/v1/";

    /**
     * The constant ID.
     */
    public static final String ID = "id";
    /**
     * The constant FIRST_NAME.
     */
    public static final String FIRST_NAME = "first_name";
    /**
     * The constant LAST_NAME.
     */
    public static final String LAST_NAME = "last_name";
    /**
     * The constant EMAIL.
     */
    public static final String EMAIL = "email";
    /**
     * The constant GOOGLE_API_KEY.
     */
    public static final String GOOGLE_API_KEY = "22099044069-napsevqt05t08fl2uj4n7so93fihq7ae.apps.googleusercontent.com";
    /**
     * The constant GENDER.
     */
    public static final String GENDER = "gender";
    /**
     * The constant DOB.
     */
    public static final String DOB = "dob";
    /**
     * The constant COUNTRY.
     */
    public static final String COUNTRY = "country";
    /**
     * The constant STATE.
     */
    public static final String STATE = "state";
    /**
     * The constant COUNTRY_CODE.
     */
    public static final String COUNTRY_CODE = "country_code";
    /**
     * The constant PHONE_NUMBER.
     */
    public static final String PHONE_NUMBER = "phone_number";
    /**
     * The constant PROFILE_PICTURE.
     */
    public static final String PROFILE_PICTURE = "profile_picture";
    /**
     * The constant CREATED_AT.
     */
    public static final String CREATED_AT = "created_at";
    /**
     * The constant UPDATED_AT.
     */
    public static final String UPDATED_AT = "updated_at";
    /**
     * The constant STATUS.
     */
    public static final String STATUS = "status";
    /**
     * The constant MESSAGE.
     */
    public static final String MESSAGE = "message";
    /**
     * The constant TOKEN.
     */
    public static final String TOKEN = "token";
    /**
     * The constant PASSWORD.
     */
    public static final String PASSWORD = "password";
    /**
     * The constant DESCRIPTION.
     */
    public static final String DESCRIPTION = "description";
    /**
     * The constant RECIPES.
     */
    public static final String RECIPES = "recipes";
    /**
     * The constant TABLE_CATEGORIES.
     */
    public static final String TABLE_CATEGORIES = "categories";
    /**
     * The constant TABLE_INGREDIENTS.
     */
    public static final String TABLE_INGREDIENTS = "ingredients";
    /**
     * The constant CATEGORY_DETAILS.
     */
    public static final String CATEGORY_DETAILS = "category_details";
    /**
     * The constant IMAGE.
     */
    public static final String IMAGE = "image";
    /**
     * The constant CALORIE_COUNT.
     */
    public static final String CALORIE_COUNT = "calorie_count";
    /**
     * The constant PROTEIN_COUNT.
     */
    public static final String PROTEIN_COUNT = "protein_count";
    /**
     * The constant CARBOHYDRATE_COUNT.
     */
    public static final String CARBOHYDRATE_COUNT = "carbohydrate_count";
    /**
     * The constant TABLE_RECIPES.
     */
    public static final String TABLE_RECIPES = "recipes";
    /**
     * The constant CATEGORY_ID.
     */
    public static final String CATEGORY_ID = "category_id";
    /**
     * The constant DIFFICULTY.
     */
    public static final String DIFFICULTY = "difficulty";
    /**
     * The constant DURATION.
     */
    public static final String DURATION = "duration";
    /**
     * The constant RECIPE_ID.
     */
    public static final String RECIPE_ID = "recipe_id";
    /**
     * The constant CAPTION.
     */
    public static final String CAPTION = "caption";
    /**
     * The constant TABLE_RECIPE_IMAGES.
     */
    public static final String TABLE_RECIPE_IMAGES = "recipe_images";
    /**
     * The constant INGREDIENT_ID.
     */
    public static final String INGREDIENT_ID = "ingredient_id";
    /**
     * The constant QUANTITY.
     */
    public static final String QUANTITY = "quantity";
    /**
     * The constant QUANTITY_SCALE.
     */
    public static final String QUANTITY_SCALE = "quantity_scale";
    /**
     * The constant TABLE_RECIPE_INGREDIENTS.
     */
    public static final String TABLE_RECIPE_INGREDIENTS = "recipe_ingredients";
    /**
     * The constant TABLE_RECIPE_STEPS.
     */
    public static final String TABLE_RECIPE_STEPS = "recipe_steps";
    /**
     * The constant TABLE_RECIPE_TOOLS.
     */
    public static final String TABLE_RECIPE_TOOLS = "recipe_tools";
    /**
     * The constant RECIPE_STEP_ID.
     */
    public static final String RECIPE_STEP_ID = "recipe_step_id";
    /**
     * The constant TOOL_ID.
     */
    public static final String TOOL_ID = "tool_id";
    /**
     * The constant TABLE_TOOLS.
     */
    public static final String TABLE_TOOLS = "tools";
    /**
     * The constant DB_NAME.
     */
    public static final String DB_NAME = "reciprada_db";
    /**
     * The constant DATA.
     */
    public static final String DATA = "data";
    /**
     * The constant IMAGES.
     */
    public static final String IMAGES = "images";
    /**
     * The constant PREPARATION_STEPS.
     */
    public static final String PREPARATION_STEPS = "preparation_steps";
    /**
     * The constant TOOLS_REQUIRED.
     */
    public static final String TOOLS_REQUIRED = "tools_required";
    /**
     * The constant REQUEST_TOKEN.
     */
    public static final String REQUEST_TOKEN = "request_token";
    /**
     * The constant DEFAULTS_SET.
     */
    public static final String DEFAULTS_SET = "defaults_set";
    /**
     * The constant INGREDIENT_DETAILS.
     */
    public static final String INGREDIENT_DETAILS = "ingredient_details";
    /**
     * The constant TOOL_DETAILS.
     */
    public static final String TOOL_DETAILS = "tool_details";
    /**
     * The constant DETAILS_LOADED.
     */
    public static final String DETAILS_LOADED = "details_loaded";
    /**
     * The constant RECIPE_HASH.
     */
    public static final int RECIPE_HASH = 10;
    /**
     * The constant CATEGORY_HASH.
     */
    public static final int CATEGORY_HASH = 20;

    /**
     * The constant LOADED_FROM_RECIPE_LIST.
     */
    public static final String LOADED_FROM_RECIPE_LIST = "loaded_from_recipe_list";
    /**
     * The constant LOADED_FROM_RECIPE_SEARCH.
     */
    public static final String LOADED_FROM_RECIPE_SEARCH = "loaded_from_recipe_search";
    /**
     * The constant LOADED_FROM_RECIPE_CATEGORY.
     */
    public static final String LOADED_FROM_RECIPE_CATEGORY = "loaded_from_recipe_category";
    /**
     * The constant API_ITEMS_LIMIT.
     */
    public static final int API_ITEMS_LIMIT = 10;
    /**
     * The constant DATABASE_PAGE_SIZE.
     */
    public static final int DATABASE_PAGE_SIZE = 5;


    //Setting this constructor as private will hide the constructor
    private GlobalConstants(){}

}
