package com.ifwaxtel.reciprada.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

//@Entity(primaryKeys = {"firstName", "lastName"})

/**
 * The type Recipe ingredient.
 */
@Entity(tableName = GlobalConstants.TABLE_RECIPE_INGREDIENTS,
        indices = {@Index(GlobalConstants.RECIPE_ID),
                @Index(GlobalConstants.INGREDIENT_ID)},
        foreignKeys = {
                @ForeignKey(entity = Recipe.class,
                        parentColumns = GlobalConstants.ID,
                        childColumns = GlobalConstants.RECIPE_ID),
                @ForeignKey(entity = Ingredient.class,
                        parentColumns = GlobalConstants.ID,
                        childColumns = GlobalConstants.INGREDIENT_ID)
        }
)
public class RecipeIngredient implements Parcelable{

    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.RECIPE_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.RECIPE_ID)
    private int recipeId;

    @SerializedName(GlobalConstants.INGREDIENT_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.INGREDIENT_ID)
    private int ingredientId;

    @SerializedName(GlobalConstants.INGREDIENT_DETAILS)
    @Expose
    @Ignore
    private Ingredient ingredient;

    @SerializedName(GlobalConstants.QUANTITY)
    @Expose
    private float quantity;

    @SerializedName(GlobalConstants.QUANTITY_SCALE)
    @Expose
    @ColumnInfo(name = GlobalConstants.QUANTITY_SCALE)
    private String quantityScale;

    /**
     * Instantiates a new Recipe ingredient.
     *
     * @param in the in
     */
    protected RecipeIngredient(Parcel in) {
        id = in.readInt();
        recipeId = in.readInt();
        ingredientId = in.readInt();
        ingredient = in.readParcelable(Ingredient.class.getClassLoader());
        quantity = in.readFloat();
        quantityScale = in.readString();
    }

    /**
     * Instantiates a new Recipe ingredient.
     */
    public RecipeIngredient() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(recipeId);
        dest.writeInt(ingredientId);
        dest.writeParcelable(ingredient, flags);
        dest.writeFloat(quantity);
        dest.writeString(quantityScale);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<RecipeIngredient> CREATOR = new Creator<RecipeIngredient>() {
        @Override
        public RecipeIngredient createFromParcel(Parcel in) {
            return new RecipeIngredient(in);
        }

        @Override
        public RecipeIngredient[] newArray(int size) {
            return new RecipeIngredient[size];
        }
    };

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets recipe id.
     *
     * @return the recipe id
     */
    public int getRecipeId() {
        return recipeId;
    }

    /**
     * Sets recipe id.
     *
     * @param recipeId the recipe id
     */
    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    /**
     * Gets ingredient id.
     *
     * @return the ingredient id
     */
    public int getIngredientId() {
        return ingredientId;
    }

    /**
     * Sets ingredient id.
     *
     * @param ingredientId the ingredient id
     */
    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    /**
     * Gets ingredient.
     *
     * @return the ingredient
     */
    public Ingredient getIngredient() {
        return ingredient;
    }

    /**
     * Sets ingredient.
     *
     * @param ingredient the ingredient
     */
    public void setIngredient(Ingredient ingredient) {
        this.ingredient = ingredient;
    }

    /**
     * Gets quantity.
     *
     * @return the quantity
     */
    public float getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity the quantity
     */
    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets quantity scale.
     *
     * @return the quantity scale
     */
    public String getQuantityScale() {
        return quantityScale;
    }

    /**
     * Sets quantity scale.
     *
     * @param quantityScale the quantity scale
     */
    public void setQuantityScale(String quantityScale) {
        this.quantityScale = quantityScale;
    }

    @NonNull
    @Override
    public String toString() {
        return "RecipeIngredient{" +
                "id=" + id +
                ", recipeId=" + recipeId +
                ", ingredientId=" + ingredientId +
                ", ingredient=" + ingredient +
                ", quantity=" + quantity +
                ", quantityScale='" + quantityScale + '\'' +
                '}';
    }
}
