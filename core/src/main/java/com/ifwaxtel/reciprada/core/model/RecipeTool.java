package com.ifwaxtel.reciprada.core.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;


/**
 * The type Recipe tool.
 */
@Entity(tableName = GlobalConstants.TABLE_RECIPE_TOOLS,
        indices = {@Index(GlobalConstants.RECIPE_ID),
                @Index(GlobalConstants.TOOL_ID)},
        foreignKeys = {
                @ForeignKey(entity = Recipe.class,
                        parentColumns = GlobalConstants.ID,
                        childColumns = GlobalConstants.RECIPE_ID),
                @ForeignKey(entity = Tool.class,
                        parentColumns = GlobalConstants.ID,
                        childColumns = GlobalConstants.TOOL_ID)
        }
)
public class RecipeTool implements Parcelable{

    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.RECIPE_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.RECIPE_ID)
    private int recipeId;

    @SerializedName(GlobalConstants.TOOL_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.TOOL_ID)
    private int toolId;

    @SerializedName(GlobalConstants.TOOL_DETAILS)
    @Expose
    @Ignore
    private Tool tool;


    /**
     * Instantiates a new Recipe tool.
     *
     * @param in the in
     */
    protected RecipeTool(Parcel in) {
        id = in.readInt();
        recipeId = in.readInt();
        toolId = in.readInt();
        tool = in.readParcelable(Tool.class.getClassLoader());
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<RecipeTool> CREATOR = new Creator<RecipeTool>() {
        @Override
        public RecipeTool createFromParcel(Parcel in) {
            return new RecipeTool(in);
        }

        @Override
        public RecipeTool[] newArray(int size) {
            return new RecipeTool[size];
        }
    };

    /**
     * Instantiates a new Recipe tool.
     */
    public RecipeTool() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(recipeId);
        dest.writeInt(toolId);
        dest.writeParcelable(tool, flags);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets recipe id.
     *
     * @return the recipe id
     */
    public int getRecipeId() {
        return recipeId;
    }

    /**
     * Sets recipe id.
     *
     * @param recipeId the recipe id
     */
    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    /**
     * Gets tool id.
     *
     * @return the tool id
     */
    public int getToolId() {
        return toolId;
    }

    /**
     * Sets tool id.
     *
     * @param toolId the tool id
     */
    public void setToolId(int toolId) {
        this.toolId = toolId;
    }

    /**
     * Gets tool.
     *
     * @return the tool
     */
    public Tool getTool() {
        return tool;
    }

    /**
     * Sets tool.
     *
     * @param tool the tool
     */
    public void setTool(Tool tool) {
        this.tool = tool;
    }

    @NonNull
    @Override
    public String toString() {
        return "RecipeTool{" +
                "id=" + id +
                ", recipeId=" + recipeId +
                ", toolId=" + toolId +
                ", tool=" + tool +
                '}';
    }
}
