package com.ifwaxtel.reciprada.core.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

/**
 * The type Recipe step.
 */
@Entity(tableName = GlobalConstants.TABLE_RECIPE_STEPS,
        indices = {@Index(GlobalConstants.RECIPE_ID)},
        foreignKeys = @ForeignKey(
                entity = Recipe.class,
                parentColumns = GlobalConstants.ID,
                childColumns = GlobalConstants.RECIPE_ID,
                onDelete = ForeignKey.CASCADE)
)
public class RecipeStep implements Parcelable{
    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.RECIPE_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.RECIPE_ID)
    private int recipeId;

    @SerializedName(GlobalConstants.IMAGE)
    @Expose
    private String image;

    @SerializedName(GlobalConstants.DESCRIPTION)
    @Expose
    private String description;

    /**
     * Instantiates a new Recipe step.
     *
     * @param in the in
     */
    protected RecipeStep(Parcel in) {
        id = in.readInt();
        recipeId = in.readInt();
        image = in.readString();
        description = in.readString();
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<RecipeStep> CREATOR = new Creator<RecipeStep>() {
        @Override
        public RecipeStep createFromParcel(Parcel in) {
            return new RecipeStep(in);
        }

        @Override
        public RecipeStep[] newArray(int size) {
            return new RecipeStep[size];
        }
    };

    /**
     * Instantiates a new Recipe step.
     */
    public RecipeStep() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(recipeId);
        dest.writeString(image);
        dest.writeString(description);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets recipe id.
     *
     * @return the recipe id
     */
    public int getRecipeId() {
        return recipeId;
    }

    /**
     * Sets recipe id.
     *
     * @param recipeId the recipe id
     */
    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @NonNull
    @Override
    public String toString() {
        return "RecipeStep{" +
                "id=" + id +
                ", recipeId=" + recipeId +
                ", image='" + image + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
