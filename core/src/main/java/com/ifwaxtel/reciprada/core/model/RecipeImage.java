package com.ifwaxtel.reciprada.core.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

/**
 * The type Recipe image.
 */
@Entity(tableName = GlobalConstants.TABLE_RECIPE_IMAGES,
        indices = {@Index(GlobalConstants.RECIPE_ID)},
        foreignKeys = @ForeignKey(entity = Recipe.class,
        parentColumns = GlobalConstants.ID,
        childColumns = GlobalConstants.RECIPE_ID))
public class RecipeImage implements Parcelable{

    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.RECIPE_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.RECIPE_ID)
    private int recipeId;

    @SerializedName(GlobalConstants.IMAGE)
    @Expose
    private String image;

    @SerializedName(GlobalConstants.CAPTION)
    @Expose
    private String caption;

    /**
     * Instantiates a new Recipe image.
     *
     * @param in the in
     */
    protected RecipeImage(Parcel in) {
        id = in.readInt();
        recipeId = in.readInt();
        image = in.readString();
        caption = in.readString();
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<RecipeImage> CREATOR = new Creator<RecipeImage>() {
        @Override
        public RecipeImage createFromParcel(Parcel in) {
            return new RecipeImage(in);
        }

        @Override
        public RecipeImage[] newArray(int size) {
            return new RecipeImage[size];
        }
    };

    /**
     * Instantiates a new Recipe image.
     */
    public RecipeImage() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(recipeId);
        dest.writeString(image);
        dest.writeString(caption);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets recipe id.
     *
     * @return the recipe id
     */
    public int getRecipeId() {
        return recipeId;
    }

    /**
     * Sets recipe id.
     *
     * @param recipeId the recipe id
     */
    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets caption.
     *
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * Sets caption.
     *
     * @param caption the caption
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    @NonNull
    @Override
    public String toString() {
        return "RecipeImage{" +
                "id=" + id +
                ", recipeId=" + recipeId +
                ", image='" + image + '\'' +
                ", caption='" + caption + '\'' +
                '}';
    }
}
