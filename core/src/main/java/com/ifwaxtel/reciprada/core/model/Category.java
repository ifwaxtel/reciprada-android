package com.ifwaxtel.reciprada.core.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

/**
 * The type Category.
 */
@Entity(tableName = GlobalConstants.TABLE_CATEGORIES)
public class Category implements Parcelable {

    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.NAME)
    @Expose
    private String name;

    @SerializedName(GlobalConstants.DESCRIPTION)
    @Expose
    private String description;

    @SerializedName(GlobalConstants.IMAGE)
    @Expose
    private String image;

    @SerializedName(GlobalConstants.RECIPES)
    @Expose
    @ColumnInfo(name = GlobalConstants.RECIPES)
    private int recipes;

    /**
     * Instantiates a new Category.
     *
     * @param in the in
     */
    @Ignore
    protected Category(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.description = in.readString();
        this.image = in.readString();
        this.recipes = in.readInt();
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    /**
     * Instantiates a new Category.
     */
    public Category() {
    }

    @Override
    public int hashCode() {
        return GlobalConstants.CATEGORY_HASH + this.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Category))
            return false;
        if (obj == this)
            return true;
        return this.getId() == ((Category) obj).getId();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeInt(recipes);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets recipes.
     *
     * @return the recipes
     */
    public int getRecipes() {
        return recipes;
    }

    /**
     * Sets recipes.
     *
     * @param recipes the recipes
     */
    public void setRecipes(int recipes) {
        this.recipes = recipes;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    @NonNull
    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", recipes=" + recipes +
                '}';
    }
}
