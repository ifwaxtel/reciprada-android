package com.ifwaxtel.reciprada.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Recipe.
 */
@Entity(tableName = GlobalConstants.TABLE_RECIPES,
        indices = {@Index(GlobalConstants.CATEGORY_ID)},
        foreignKeys = @ForeignKey(entity = Category.class,
        parentColumns = GlobalConstants.ID,
        childColumns = GlobalConstants.CATEGORY_ID))
public class Recipe implements Parcelable{

    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.CATEGORY_ID)
    @Expose
    @ColumnInfo(name = GlobalConstants.CATEGORY_ID)
    private int categoryId;

    @SerializedName(GlobalConstants.NAME)
    @Expose
    private String name;

    @SerializedName(GlobalConstants.DESCRIPTION)
    @Expose
    private String description;

    @SerializedName(GlobalConstants.DIFFICULTY)
    @Expose
    private String difficulty;

    @SerializedName(GlobalConstants.DURATION)
    @Expose
    private int duration;

    @SerializedName(GlobalConstants.CATEGORY_DETAILS)
    @Expose
    @Ignore
    private Category category;

    @SerializedName(GlobalConstants.IMAGES)
    @Expose
    @Ignore
    private List<RecipeImage> recipeImages = new ArrayList<>();

    @SerializedName(GlobalConstants.TABLE_INGREDIENTS)
    @Expose
    @Ignore
    private List<RecipeIngredient> recipeIngredients = new ArrayList<>();

    @SerializedName(GlobalConstants.PREPARATION_STEPS)
    @Expose
    @Ignore
    private List<RecipeStep> recipeSteps = new ArrayList<>();

    @SerializedName(GlobalConstants.TOOLS_REQUIRED)
    @Expose
    @Ignore
    private List<RecipeTool> recipeTools = new ArrayList<>();

    @ColumnInfo(name = GlobalConstants.DETAILS_LOADED)
    private boolean detailsLoaded;

    private boolean favourite;
    private String loadedFrom;

    /**
     * Instantiates a new Recipe.
     *
     * @param in the in
     */
    protected Recipe(Parcel in) {
        id = in.readInt();
        categoryId = in.readInt();
        name = in.readString();
        description = in.readString();
        difficulty = in.readString();
        duration = in.readInt();
        category = in.readParcelable(Category.class.getClassLoader());
        recipeImages = in.createTypedArrayList(RecipeImage.CREATOR);
        recipeIngredients = in.createTypedArrayList(RecipeIngredient.CREATOR);
        recipeSteps = in.createTypedArrayList(RecipeStep.CREATOR);
        recipeTools = in.createTypedArrayList(RecipeTool.CREATOR);
        detailsLoaded = in.readByte() != 0;
        favourite = in.readByte() != 0;
        loadedFrom = in.readString();
    }

    /**
     * Instantiates a new Recipe.
     */
    public Recipe() {
    }

    @Override
    public int hashCode() {
        return GlobalConstants.RECIPE_HASH + this.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Recipe))
            return false;
        if (obj == this)
            return true;
        return this.getId() == ((Recipe) obj).getId();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(categoryId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(difficulty);
        dest.writeInt(duration);
        dest.writeParcelable(category, flags);
        dest.writeTypedList(recipeImages);
        dest.writeTypedList(recipeIngredients);
        dest.writeTypedList(recipeSteps);
        dest.writeTypedList(recipeTools);
        dest.writeByte((byte) (detailsLoaded ? 1 : 0));
        dest.writeByte((byte) (favourite ? 1 : 0));
        dest.writeString(loadedFrom);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets category id.
     *
     * @return the category id
     */
    public int getCategoryId() {
        return categoryId;
    }

    /**
     * Sets category id.
     *
     * @param categoryId the category id
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets difficulty.
     *
     * @return the difficulty
     */
    public String getDifficulty() {
        return difficulty;
    }

    /**
     * Sets difficulty.
     *
     * @param difficulty the difficulty
     */
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * Gets duration.
     *
     * @return the duration
     */
    public int getDuration() {
        return duration;
    }

    /**
     * Sets duration.
     *
     * @param duration the duration
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * Gets recipe images.
     *
     * @return the recipe images
     */
    public List<RecipeImage> getRecipeImages() {
        return recipeImages;
    }

    /**
     * Sets recipe images.
     *
     * @param recipeImages the recipe images
     */
    public void setRecipeImages(List<RecipeImage> recipeImages) {
        this.recipeImages = recipeImages;
    }

    /**
     * Gets recipe ingredients.
     *
     * @return the recipe ingredients
     */
    public List<RecipeIngredient> getRecipeIngredients() {
        return recipeIngredients;
    }

    /**
     * Sets recipe ingredients.
     *
     * @param recipeIngredients the recipe ingredients
     */
    public void setRecipeIngredients(List<RecipeIngredient> recipeIngredients) {
        this.recipeIngredients = recipeIngredients;
    }

    /**
     * Gets recipe steps.
     *
     * @return the recipe steps
     */
    public List<RecipeStep> getRecipeSteps() {
        return recipeSteps;
    }

    /**
     * Sets recipe steps.
     *
     * @param recipeSteps the recipe steps
     */
    public void setRecipeSteps(List<RecipeStep> recipeSteps) {
        this.recipeSteps = recipeSteps;
    }

    /**
     * Gets recipe tools.
     *
     * @return the recipe tools
     */
    public List<RecipeTool> getRecipeTools() {
        return recipeTools;
    }

    /**
     * Sets recipe tools.
     *
     * @param recipeTools the recipe tools
     */
    public void setRecipeTools(List<RecipeTool> recipeTools) {
        this.recipeTools = recipeTools;
    }

    /**
     * Is details loaded boolean.
     *
     * @return the boolean
     */
    public boolean isDetailsLoaded() {
        return detailsLoaded;
    }

    /**
     * Sets details loaded.
     *
     * @param detailsLoaded the details loaded
     */
    public void setDetailsLoaded(boolean detailsLoaded) {
        this.detailsLoaded = detailsLoaded;
    }

    /**
     * Is favourite boolean.
     *
     * @return the boolean
     */
    public boolean isFavourite() {
        return favourite;
    }

    /**
     * Sets favourite.
     *
     * @param favourite the favourite
     */
    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    /**
     * Gets loaded from.
     *
     * @return the loaded from
     */
    public String getLoadedFrom() {
        return loadedFrom;
    }

    /**
     * Sets loaded from.
     *
     * @param loadedFrom the loaded from
     */
    public void setLoadedFrom(String loadedFrom) {
        this.loadedFrom = loadedFrom;
    }

    @NonNull
    @Override
    public String toString() {
        return "Recipe{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", duration=" + duration +
                ", category=" + category +
                ", recipeImages=" + recipeImages +
                ", recipeIngredients=" + recipeIngredients +
                ", recipeSteps=" + recipeSteps +
                ", recipeTools=" + recipeTools +
                ", detailsLoaded=" + detailsLoaded +
                '}';
    }


}
