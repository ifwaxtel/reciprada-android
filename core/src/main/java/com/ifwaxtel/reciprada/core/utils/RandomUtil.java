package com.ifwaxtel.reciprada.core.utils;

import java.util.Random;

/**
 * The type Random util.
 */
public class RandomUtil {

    /**
     * Random password string.
     *
     * @param passwordLength the password length
     * @return the string
     */
    public static String randomPassword(int passwordLength)
    {

        //A strong password should have capital characters,
        //small characters, numbers and symbols
        String capitalChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String smallChars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String symbols = "!@#$%^&*_=+-/.?<>)";

        String strings = capitalChars + smallChars + numbers + symbols;

        Random random = new Random();

        char[] password = new char[passwordLength];

        for (int i = 0; i < passwordLength; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] = strings.charAt(random.nextInt(strings.length()));

        }

        return String.valueOf(password);
    }
}
