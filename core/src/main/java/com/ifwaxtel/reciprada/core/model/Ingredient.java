package com.ifwaxtel.reciprada.core.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ifwaxtel.reciprada.core.constants.GlobalConstants;

/**
 * The type Ingredient.
 */
@Entity(tableName = GlobalConstants.TABLE_INGREDIENTS)
public class Ingredient implements Parcelable {
    @PrimaryKey
    @SerializedName(GlobalConstants.ID)
    @Expose
    private int id;

    @SerializedName(GlobalConstants.NAME)
    @Expose
    private String name;

    @SerializedName(GlobalConstants.DESCRIPTION)
    @Expose
    private String description;

    @SerializedName(GlobalConstants.IMAGE)
    @Expose
    private String image;

    @SerializedName(GlobalConstants.CALORIE_COUNT)
    @Expose
    @ColumnInfo(name = GlobalConstants.CALORIE_COUNT)
    private float calorieCount;

    @SerializedName(GlobalConstants.PROTEIN_COUNT)
    @Expose
    @ColumnInfo(name = GlobalConstants.PROTEIN_COUNT)
    private float proteinCount;

    @SerializedName(GlobalConstants.CARBOHYDRATE_COUNT)
    @Expose
    @ColumnInfo(name = GlobalConstants.CARBOHYDRATE_COUNT)
    private float carbohydrateCount;


    /**
     * Instantiates a new Ingredient.
     *
     * @param in the in
     */
    protected Ingredient(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        image = in.readString();
        calorieCount = in.readFloat();
        proteinCount = in.readFloat();
        carbohydrateCount = in.readFloat();
    }

    /**
     * The constant CREATOR.
     */
    public static final Creator<Ingredient> CREATOR = new Creator<Ingredient>() {
        @Override
        public Ingredient createFromParcel(Parcel in) {
            return new Ingredient(in);
        }

        @Override
        public Ingredient[] newArray(int size) {
            return new Ingredient[size];
        }
    };

    /**
     * Instantiates a new Ingredient.
     */
    public Ingredient() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeFloat(calorieCount);
        dest.writeFloat(proteinCount);
        dest.writeFloat(carbohydrateCount);
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets calorie count.
     *
     * @return the calorie count
     */
    public float getCalorieCount() {
        return calorieCount;
    }

    /**
     * Sets calorie count.
     *
     * @param calorieCount the calorie count
     */
    public void setCalorieCount(float calorieCount) {
        this.calorieCount = calorieCount;
    }

    /**
     * Gets protein count.
     *
     * @return the protein count
     */
    public float getProteinCount() {
        return proteinCount;
    }

    /**
     * Sets protein count.
     *
     * @param proteinCount the protein count
     */
    public void setProteinCount(float proteinCount) {
        this.proteinCount = proteinCount;
    }

    /**
     * Gets carbohydrate count.
     *
     * @return the carbohydrate count
     */
    public float getCarbohydrateCount() {
        return carbohydrateCount;
    }

    /**
     * Sets carbohydrate count.
     *
     * @param carbohydrateCount the carbohydrate count
     */
    public void setCarbohydrateCount(float carbohydrateCount) {
        this.carbohydrateCount = carbohydrateCount;
    }

    @NonNull
    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", calorieCount=" + calorieCount +
                ", proteinCount=" + proteinCount +
                ", carbohydrateCount=" + carbohydrateCount +
                '}';
    }
}
